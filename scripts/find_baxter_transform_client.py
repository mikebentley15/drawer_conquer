#!/usr/bin/env python

import sys
import rospy
from drawer_conquer.srv import FindBaxterTransform
from drawer_conquer.msg import BaxterConfiguration

import tf.transformations
import numpy as np

def print_rotation_matrices(transforms):
    def q_to_array(quat):
        return np.asarray([quat.x, quat.y, quat.z, quat.w])
    def q_to_mat(quat):
        return tf.transformations.quaternion_matrix(q_to_array(quat))
    def q_to_euler(quat):
        return tf.transformations.euler_from_quaternion(q_to_array(quat))
    def print_mat(name, quat):
        print name
        print q_to_mat(quat)
        print
    def print_euler(name, quat):
        print name
        print q_to_euler(quat)
        print
    print_euler('left.to_s0',       transforms.left.to_s0.rotation)
    print_euler('left.to_s1',       transforms.left.to_s1.rotation)
    print_euler('left.to_e0',       transforms.left.to_e0.rotation)
    print_euler('left.to_e1',       transforms.left.to_e1.rotation)
    print_euler('left.to_w0',       transforms.left.to_w0.rotation)
    print_euler('left.to_w1',       transforms.left.to_w1.rotation)
    print_euler('left.to_w2',       transforms.left.to_w2.rotation)
    print_euler('left.to_gripper1', transforms.left.to_gripper1.rotation)
    print_euler('left.to_gripper2', transforms.left.to_gripper2.rotation)

    print_euler('right.to_s0',       transforms.right.to_s0.rotation)
    print_euler('right.to_s1',       transforms.right.to_s1.rotation)
    print_euler('right.to_e0',       transforms.right.to_e0.rotation)
    print_euler('right.to_e1',       transforms.right.to_e1.rotation)
    print_euler('right.to_w0',       transforms.right.to_w0.rotation)
    print_euler('right.to_w1',       transforms.right.to_w1.rotation)
    print_euler('right.to_w2',       transforms.right.to_w2.rotation)
    print_euler('right.to_gripper1', transforms.right.to_gripper1.rotation)
    print_euler('right.to_gripper2', transforms.right.to_gripper2.rotation)
    #transforms.left.to_s0.rotation = quat1
    #transforms.left.to_s0.translation = trans1
    #transforms.left.to_s1.rotation = quat2
    #transforms.left.to_s1.translation = trans2
    #transforms.left.to_e0.rotation = quat3
    #transforms.left.to_e0.translation = trans3
    #transforms.left.to_e1.rotation = quat4
    #transforms.left.to_e1.translation = trans4
    #transforms.left.to_w0.rotation = quat5
    #transforms.left.to_w0.translation = trans5
    #transforms.left.to_w1.rotation = quat6
    #transforms.left.to_w1.translation = trans6
    #transforms.left.to_w2.rotation = quat7
    #transforms.left.to_w2.translation = trans7
    #transforms.left.to_gripper1.rotation = quat7
    #transforms.left.to_gripper1.translation = trans8
    #transforms.left.to_gripper2.rotation = quat7
    #transforms.left.to_gripper2.translation = trans8_2

def main(arguments):
    config = BaxterConfiguration()
    rospy.wait_for_service('find_baxter_transform')
    try:
        find_baxter_transform = rospy.ServiceProxy('find_baxter_transform',
                FindBaxterTransform)
        response = find_baxter_transform(config)
        print 'Response:', response
        print_rotation_matrices(response.transforms)
    except rospy.ServiceException, e:
        print 'Service call failed:', e

if __name__ == '__main__':
    main(sys.argv[1:])
