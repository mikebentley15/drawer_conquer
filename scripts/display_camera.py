#!/usr/bin/env python
'A script capable of playing loops of image sequences on the Baxter display.'

import argparse
import cv2
import cv_bridge
import numpy as np
import os
import rospy
import sys
import time

from collections import deque

from sensor_msgs.msg import Image

script_dir = os.path.dirname(__file__)

screen_width = 1024
screen_height = 600
hand_cam_width = 640
hand_cam_height = 400

# Create the buffers and zero them out
l_image = np.zeros((hand_cam_height, hand_cam_width, 3), np.uint8)
r_image = np.zeros((hand_cam_height, hand_cam_width, 3), np.uint8)
l_image_ring = deque(maxlen=3)
r_image_ring = deque(maxlen=3)
hand_scaling = min((screen_width/2.0) / hand_cam_width,
                   screen_height / float(hand_cam_height))
hand_resize_size = (
    int(hand_scaling * hand_cam_width),
    int(hand_scaling * hand_cam_height),
    )

total_image = np.zeros((screen_height, screen_width, 3), np.uint8)
smiley = cv2.imread(os.path.join(script_dir, '..', 'images', 'smiley-face.jpg'))
smiley_small = cv2.resize(smiley, (screen_width/2, screen_height/2))
total_image[smiley_small.shape[0]:, smiley_small.shape[1]:, :] = smiley_small

def callback(msg, ring):
    im = cv_bridge.CvBridge().imgmsg_to_cv2(msg)
    ring.appendleft(im[:,:,:3])

def try_publish(pub):
    'Publishes at a particular frame rate if there is something to publish'
    has_new_stuff = False
    w, h = hand_resize_size

    try:
        l_image = l_image_ring.pop()
    except IndexError:
        pass
    else:
        has_new_stuff = True
        resized = cv2.resize(l_image, hand_resize_size)
        total_image[0:h, 0:w, :] = resized

    try:
        r_image = r_image_ring.pop()
    except IndexError:
        pass
    else:
        has_new_stuff = True
        resized = cv2.resize(r_image, hand_resize_size)
        total_image[0:h, w:2*w, :] = resized

    if has_new_stuff:
        #print 'Publishing new image'
        msg = cv_bridge.CvBridge().cv2_to_imgmsg(total_image, encoding='bgr8')
        pub.publish(msg)

def main(arguments):
    'Main entry point'

    # Parse arguments
    #parser = argparse.ArgumentParser(
    #    description='Plays images on the baxter screen')
    #parser.add_argument('-r', '--repeats', type=int, default=1,
    #    help='Number of times to repeat the image sequence')
    #parser.add_argument('-s', '--sleep', type=float, default=0.033,
    #    help='How long to wait between frames')
    #parser.add_argument('imagepath', nargs='+', help='images to display')
    #args = parser.parse_args(arguments)

    # Initialize the ROS connection
    rospy.init_node('displayer', anonymous=True)
    pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=2)
    l_sub = rospy.Subscriber('/cameras/left_hand_camera/image', Image,
        callback, l_image_ring, queue_size=1, tcp_nodelay=True)
    r_sub = rospy.Subscriber('/cameras/right_hand_camera/image', Image,
        callback, r_image_ring, queue_size=1, tcp_nodelay=True)

    rate = rospy.Rate(25) # Hz
    while not rospy.is_shutdown():
        try_publish(pub)
        rate.sleep()

    # Load the images into memory
    #msgs = []
    #for path in args.imagepath:
    #    image = cv2.imread(path)
    #    msgs.append(cv_bridge.CvBridge().cv2_to_imgmsg(image, encoding='bgr8'))

    ## Publish the images
    #for _ in xrange(args.repeats):
    #    for msg in msgs:
    #        if rospy.is_shutdown():
    #            sys.exit(0)
    #        before = time.clock()
    #        pub.publish(msg)
    #        after = time.clock()
    #        difference = after - before
    #        rospy.sleep(args.sleep - difference)

if __name__ == '__main__':
    main(sys.argv[1:])
