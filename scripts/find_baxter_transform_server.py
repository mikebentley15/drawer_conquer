#!/usr/bin/env python

import rospy
from drawer_conquer.srv import FindBaxterTransform
from drawer_conquer.msg import BaxterTransforms, BaxterConfiguration
from geometry_msgs.msg import Vector3, Quaternion
from fk_solver.forward_kinematics import ForwardKinematics
import sys

from math import sin, cos
import math
import numpy as np
from tf import transformations

pi = 3.1415925

def handle_find_transform(req):
    print 'Baxter Configuration Received:'
    print req.config
    transforms = BaxterTransforms()

    gripper_min_width = 0.06152
    gripper_max_width = gripper_min_width + 0.030555
    calc_gripper_width = lambda percentage: (gripper_max_width - gripper_min_width) * percentage + gripper_min_width

    #
    # Left arm
    #

    left_fk = ForwardKinematics('left')
    left_angles = {
        'left_s0': req.config.left.s0,
        'left_s1': req.config.left.s1,
        'left_e0': req.config.left.e0,
        'left_e1': req.config.left.e1,
        'left_w0': req.config.left.w0,
        'left_w1': req.config.left.w1,
        'left_w2': req.config.left.w2,
        }
    left_transforms = left_fk.forward_position_kinematics(left_angles)
    left_gripper_transform = left_transforms.pop()

    transforms.left.to_s0 = left_transforms[0]
    transforms.left.to_s1 = left_transforms[1]
    transforms.left.to_e0 = left_transforms[2]
    transforms.left.to_e1 = left_transforms[3]
    transforms.left.to_w0 = left_transforms[4]
    transforms.left.to_w1 = left_transforms[5]
    transforms.left.to_w2 = left_transforms[6]
    transforms.left.to_gripper1.rotation = left_gripper_transform.rotation
    transforms.left.to_gripper2.rotation = left_gripper_transform.rotation

    shift = np.asarray([
        0,
        calc_gripper_width(req.config.left.gripper_open_percentage) / 2.,
        0,
        1,
        ])
    left_gripper_translation = np.asarray([
        left_gripper_transform.translation.x,
        left_gripper_transform.translation.y,
        left_gripper_transform.translation.z,
        1,
        ])
    q = left_gripper_transform.rotation
    left_gripper_rotation = transformations.quaternion_matrix([q.x, q.y, q.z, q.w])
    trans = left_gripper_translation + left_gripper_rotation.dot(shift)
    transforms.left.to_gripper1.translation.x = trans[0]
    transforms.left.to_gripper1.translation.y = trans[1]
    transforms.left.to_gripper1.translation.z = trans[2]
    trans = left_gripper_translation - left_gripper_rotation.dot(shift)
    transforms.left.to_gripper2.translation.x = trans[0]
    transforms.left.to_gripper2.translation.y = trans[1]
    transforms.left.to_gripper2.translation.z = trans[2]

    #
    # Right arm
    #

    right_fk = ForwardKinematics('right')
    right_angles = {
        'right_s0': req.config.right.s0,
        'right_s1': req.config.right.s1,
        'right_e0': req.config.right.e0,
        'right_e1': req.config.right.e1,
        'right_w0': req.config.right.w0,
        'right_w1': req.config.right.w1,
        'right_w2': req.config.right.w2,
        }
    right_transforms = right_fk.forward_position_kinematics(right_angles)
    right_gripper_transform = right_transforms.pop()

    transforms.right.to_s0 = right_transforms[0]
    transforms.right.to_s1 = right_transforms[1]
    transforms.right.to_e0 = right_transforms[2]
    transforms.right.to_e1 = right_transforms[3]
    transforms.right.to_w0 = right_transforms[4]
    transforms.right.to_w1 = right_transforms[5]
    transforms.right.to_w2 = right_transforms[6]
    transforms.right.to_gripper1.rotation = right_gripper_transform.rotation
    transforms.right.to_gripper2.rotation = right_gripper_transform.rotation

    right_gripper_translation = np.asarray([
        right_gripper_transform.translation.x,
        right_gripper_transform.translation.y,
        right_gripper_transform.translation.z,
        1,
        ])
    q = right_gripper_transform.rotation
    right_gripper_rotation = transformations.quaternion_matrix([q.x, q.y, q.z, q.w])
    trans = right_gripper_translation + right_gripper_rotation.dot(shift)
    transforms.right.to_gripper1.translation.x = trans[0]
    transforms.right.to_gripper1.translation.y = trans[1]
    transforms.right.to_gripper1.translation.z = trans[2]
    trans = right_gripper_translation - right_gripper_rotation.dot(shift)
    transforms.right.to_gripper2.translation.x = trans[0]
    transforms.right.to_gripper2.translation.y = trans[1]
    transforms.right.to_gripper2.translation.z = trans[2]

    return transforms


def convert_t(t):
    return np.asmatrix(np.array([[t[0],0,0],[0,t[1],0],[0,0,t[2]]]))

def old_handle_find_transform(req):
    print 'Baxter Configuration Received:'
    print req.config
    transforms = BaxterTransforms()

    # Your logic here Keith to fill in these transforms
    ## Left hand:
    theta1 = req.config.left.s0
    theta2 = req.config.left.s1
    theta3 = req.config.left.e0
    theta4 = req.config.left.e1
    theta5 = req.config.left.w0
    theta6 = req.config.left.w1
    theta7 = req.config.left.w2
    open_perc = req.config.left.gripper_open_percentage
    #####  ADD CORRECT MAX GRIPPER SIZE FOR CORRECT RESULTS
    gripper_max = 0.092075
    gripper_min = 0.06152
    # linear model:
    #y = mx+b
    # 0.092075 = m(100)+b
    # 0.06152 = m(0) + b
    # m = (0.092075-b)/100
    # b = 0.06152
    # m = 0.00030555
    gripper_width = 0.00030555*open_perc+0.06152
    
    
    R_base_to_left_arm_mount = np.asmatrix(np.array([[0.707107,-0.707107,0],[0.707107,0.707107,0],[0,0,1]]))
    
    R01 = np.asmatrix(np.array([[cos(theta1),-sin(theta1),0],[sin(theta1),cos(theta1),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,1],[0,-1,0]]))
    R12 = np.asmatrix(np.array([[cos(theta2+pi/2),-sin(theta2+pi/2),0],[sin(theta2+pi/2),cos(theta2+pi/2),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,-1],[0,1,0]]))
    R23 = np.asmatrix(np.array([[cos(theta3),-sin(theta3),0],[sin(theta3),cos(theta3),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,1],[0,-1,0]]))
    R34 = np.asmatrix(np.array([[cos(theta4),-sin(theta4),0],[sin(theta4),cos(theta4),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,-1],[0,1,0]]))
    R45 = np.asmatrix(np.array([[cos(theta5),-sin(theta5),0],[sin(theta5),cos(theta5),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,1],[0,-1,0]]))
    R56 = np.asmatrix(np.array([[cos(theta6),-sin(theta6),0],[sin(theta6),cos(theta6),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,-1],[0,1,0]]))
    R67 = np.asmatrix(np.array([[cos(theta7),-sin(theta7),0],[sin(theta7),cos(theta7),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,1,0],[0,0,1]]))
    R00 = R_base_to_left_arm_mount

    R01 = R_base_to_left_arm_mount*R01
    R02 = R01*R12
    R03 = R02*R23
    R04 = R03*R34
    R05 = R04*R45
    R06 = R05*R56
    R07 = R06*R67

    t01 = [0.069,0,0.27] # along x1, z1
    t12 = [0,0,0]
    t23 = [0.069,0,0.364]
    t34 = [0,0,0]
    t45 = [0.01,0,0.375]
    t56 = [0,0,0]
    t67 = [0,0,0.28]

    t1z = [0,0,0.27]
    t1x = [0.069,0,0]
    t2z = [0,0,0]
    t2x = [0,0,0]
    t3z = [0,0,0.364]
    t3x = [0.069,0,0]
    t4z = [0,0,0]
    t4x = [0,0,0]
    t5z = [0,0,0.375]
    t5x = [0.01,0,0]
    t6z = [0,0,0]
    t6x = [0,0,0]
    t7z = [0,0,0.28]
    t7x = [0,0,0]
    #t8x = [0,0,gripper_max]

    t0z = [0,0,0.0118588]
    t0x = [-0.024645,0,0]
    t0y = [0,0.219645,0,0]

    xyz_0 = convert_t(t0x)+convert_t(t0z)+convert_t(t0y)+convert_t([0,0,t1z[2]/2])  #
    xyz_1 = xyz_0+R01*convert_t(t1x)+convert_t([0,0,t1z[2]/2]) # s0
    xyz_2 = xyz_1+R02*convert_t(t2x)+R01*convert_t(t2z)+R02*convert_t([0,0,t3z[2]/2]) # s1
    xyz_3 = xyz_2+R03*convert_t(t3x)+R02*convert_t([0,0,t3z[2]/2]) # e0
    xyz_4 = xyz_3+R04*convert_t(t4x)+R03*convert_t(t4z)+R04*convert_t([0,0,t5z[2]/2]) # e1
    xyz_5 = xyz_4+R05*convert_t(t5x)+R04*convert_t([0,0,t5z[2]/2]) # w0
    xyz_6 = xyz_5+R06*convert_t(t6x)+R05*convert_t(t6z)+R06*convert_t([0,0,t7z[2]/2]) # w1
    xyz_7 = xyz_6+R07*convert_t(t7x)+R06*convert_t([0,0,t7z[2]/2]) # w2
#    print 'Gripper width: ', gripper_width
    xyz_8 = xyz_7+R07*convert_t([0,gripper_width/2,0])+R07*convert_t([0,0,0.09525/2,0,0])
    xyz_8_2 = xyz_7-R07*convert_t([0,gripper_width/2,0])+R07*convert_t([0,0,0.09525/2,0,0])
    #xyz_8 = xyz_6+R07*(convert_t(t7x)+convert_t(t8x))+R06*convert_t(t7z)

    xyz_new = []
    xyz0 = []
    xyz1 = []
    xyz2 = []
    xyz3 = []
    xyz4 = []
    xyz5 = []
    xyz6 = []
    xyz7 = []
    xyz8 = []
    xyz8_2 = []
    for a in range(0,3):
        val = 0
        val1 = 0
        val2 = 0
        val3 = 0
        val4 = 0
        val5 = 0
        val6 = 0
        val7 = 0
        val8 = 0
        val8_2 = 0
        for b in range(0,3):
            val+=xyz_0[a,b]
            val1+=xyz_1[a,b]
            val2+=xyz_2[a,b]
            val3+=xyz_3[a,b]
            val4+=xyz_4[a,b]
            val5+=xyz_5[a,b]
            val6+=xyz_6[a,b]
            val7+=xyz_7[a,b]
            val8+=xyz_8[a,b]
            val8_2+=xyz_8_2[a,b]
        xyz0.append(val)
        xyz1.append(val1)
        xyz2.append(val2)
        xyz3.append(val3)
        xyz4.append(val4)
        xyz5.append(val5)
        xyz6.append(val6)
        xyz7.append(val7)
        xyz8.append(val8)
        xyz8_2.append(val8_2)
    
    xyz_0 = xyz_0

    R00 = R00*np.asmatrix(np.array([[0,-1,0],[1,0,0],[0,0,1]])) # base
    R01 = R01 # s0
    R02 = R02 # s1
    R03 = R03 # e0
    R04 = R04 # e1
    R05 = R05 # w0
    R06 = R06 # w1
    R07 = R07 # w2
    
#    trans0 = Vector3(xyz0[0],xyz0[1],xyz0[2])
    trans1 = Vector3(xyz1[0],xyz1[1],xyz1[2])
    trans2 = Vector3(xyz2[0],xyz2[1],xyz2[2])
    trans3 = Vector3(xyz3[0],xyz3[1],xyz3[2])
    trans4 = Vector3(xyz4[0],xyz4[1],xyz4[2])
    trans5 = Vector3(xyz5[0],xyz5[1],xyz5[2])
    trans6 = Vector3(xyz6[0],xyz6[1],xyz6[2])
    trans7 = Vector3(xyz7[0],xyz7[1],xyz7[2])
    trans8 = Vector3(xyz8[0],xyz8[1],xyz8[2])
    trans8_2 = Vector3(xyz8_2[0],xyz8_2[1],xyz8_2[2])
    
    def quat_calc(R):
        newR = np.zeros([4,4])
        newR[:3,:3] = R
        newR[3,3] = 1
        quat = transformations.quaternion_from_matrix(newR)
        return Quaternion(quat[0], quat[1], quat[2], quat[3])

    quat0 = quat_calc(R00)
    quat1 = quat_calc(R01)
    quat2 = quat_calc(R02)
    quat3 = quat_calc(R03)
    quat4 = quat_calc(R04)
    quat5 = quat_calc(R05)
    quat6 = quat_calc(R06)
    quat7 = quat_calc(R07)

    transforms.left.to_s0.rotation = quat1
    transforms.left.to_s0.translation = trans1
    transforms.left.to_s1.rotation = quat2
    transforms.left.to_s1.translation = trans2
    transforms.left.to_e0.rotation = quat3
    transforms.left.to_e0.translation = trans3
    transforms.left.to_e1.rotation = quat4
    transforms.left.to_e1.translation = trans4
    transforms.left.to_w0.rotation = quat5
    transforms.left.to_w0.translation = trans5
    transforms.left.to_w1.rotation = quat6
    transforms.left.to_w1.translation = trans6
    transforms.left.to_w2.rotation = quat7
    transforms.left.to_w2.translation = trans7
    transforms.left.to_gripper1.rotation = quat7
    transforms.left.to_gripper1.translation = trans8
    transforms.left.to_gripper2.rotation = quat7
    transforms.left.to_gripper2.translation = trans8_2
    
    
    ## Right hand:
    theta1 = req.config.right.s0
    theta2 = req.config.right.s1
    theta3 = req.config.right.e0
    theta4 = req.config.right.e1
    theta5 = req.config.right.w0
    theta6 = req.config.right.w1
    theta7 = req.config.right.w2
    open_perc = req.config.right.gripper_open_percentage
    
    gripper_width = 0.00030555*open_perc+0.06152
    
    R_base_to_left_arm_mount = np.asmatrix(np.array([[0.707107,0.707107,0],[-0.707107,0.707107,0],[0,0,1]]))
    
    R01 = np.asmatrix(np.array([[cos(theta1),-sin(theta1),0],[sin(theta1),cos(theta1),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,1],[0,-1,0]]))
    R12 = np.asmatrix(np.array([[cos(theta2+pi/2),-sin(theta2+pi/2),0],[sin(theta2+pi/2),cos(theta2+pi/2),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,-1],[0,1,0]]))
    R23 = np.asmatrix(np.array([[cos(theta3),-sin(theta3),0],[sin(theta3),cos(theta3),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,1],[0,-1,0]]))
    R34 = np.asmatrix(np.array([[cos(theta4),-sin(theta4),0],[sin(theta4),cos(theta4),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,-1],[0,1,0]]))
    R45 = np.asmatrix(np.array([[cos(theta5),-sin(theta5),0],[sin(theta5),cos(theta5),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,1],[0,-1,0]]))
    R56 = np.asmatrix(np.array([[cos(theta6),-sin(theta6),0],[sin(theta6),cos(theta6),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,0,-1],[0,1,0]]))
    R67 = np.asmatrix(np.array([[cos(theta7),-sin(theta7),0],[sin(theta7),cos(theta7),0],[0,0,1]]))*np.asmatrix(np.array([[1,0,0],[0,1,0],[0,0,1]]))
    R00 = R_base_to_left_arm_mount

    R01 = R_base_to_left_arm_mount*R01
    R02 = R01*R12
    R03 = R02*R23
    R04 = R03*R34
    R05 = R04*R45
    R06 = R05*R56
    R07 = R06*R67

    t01 = [0.069,0,0.27] # along x1, z1
    t12 = [0,0,0]
    t23 = [0.069,0,0.364]
    t34 = [0,0,0]
    t45 = [0.01,0,0.375]
    t56 = [0,0,0]
    t67 = [0,0,0.28]

    t1z = [0,0,0.27]
    t1x = [0.069,0,0]
    t2z = [0,0,0]
    t2x = [0,0,0]
    t3z = [0,0,0.364]
    t3x = [0.069,0,0]
    t4z = [0,0,0]
    t4x = [0,0,0]
    t5z = [0,0,0.375]
    t5x = [0.01,0,0]
    t6z = [0,0,0]
    t6x = [0,0,0]
    t7z = [0,0,0.28]
    t7x = [0,0,0]
    #t8x = [0,0,gripper_max]

    t0z = [0,0,0.0118588]
    t0x = [-0.024645,0,0]
    t0y = [0,-0.219645,0]

    xyz_0 = convert_t(t0x)+convert_t(t0z)+convert_t(t0y)+convert_t([0,0,t1z[2]/2])  #
    xyz_1 = xyz_0+R01*convert_t(t1x)+convert_t([0,0,t1z[2]/2]) # s0
    xyz_2 = xyz_1+R02*convert_t(t2x)+R01*convert_t(t2z)+R02*convert_t([0,0,t3z[2]/2]) # s1
    xyz_3 = xyz_2+R03*convert_t(t3x)+R02*convert_t([0,0,t3z[2]/2]) # e0
    xyz_4 = xyz_3+R04*convert_t(t4x)+R03*convert_t(t4z)+R04*convert_t([0,0,t5z[2]/2]) # e1
    xyz_5 = xyz_4+R05*convert_t(t5x)+R04*convert_t([0,0,t5z[2]/2]) # w0
    xyz_6 = xyz_5+R06*convert_t(t6x)+R05*convert_t(t6z)+R06*convert_t([0,0,t7z[2]/2]) # w1
    xyz_7 = xyz_6+R07*convert_t(t7x)+R06*convert_t([0,0,t7z[2]/2]) # w2
#    print 'Gripper width: ', gripper_width
    xyz_8 = xyz_7+R07*convert_t([0,gripper_width/2,0])+R07*convert_t([0,0,0.09525/2,0,0])
    xyz_8_2 = xyz_7-R07*convert_t([0,gripper_width/2,0])+R07*convert_t([0,0,0.09525/2,0,0])

    xyz_new = []
    xyz0 = []
    xyz1 = []
    xyz2 = []
    xyz3 = []
    xyz4 = []
    xyz5 = []
    xyz6 = []
    xyz7 = []
    xyz8 = []
    xyz8_2 = []
    for a in range(0,3):
        val = 0
        val1 = 0
        val2 = 0
        val3 = 0
        val4 = 0
        val5 = 0
        val6 = 0
        val7 = 0
        val8 = 0
        val8_2 = 0
        for b in range(0,3):
            val+=xyz_0[a,b]
            val1+=xyz_1[a,b]
            val2+=xyz_2[a,b]
            val3+=xyz_3[a,b]
            val4+=xyz_4[a,b]
            val5+=xyz_5[a,b]
            val6+=xyz_6[a,b]
            val7+=xyz_7[a,b]
            val8+=xyz_8[a,b]
            val8_2+=xyz_8_2[a,b]
        xyz0.append(val)
        xyz1.append(val1)
        xyz2.append(val2)
        xyz3.append(val3)
        xyz4.append(val4)
        xyz5.append(val5)
        xyz6.append(val6)
        xyz7.append(val7)
        xyz8.append(val8)
        xyz8_2.append(val8_2)
    
    xyz_0 = xyz_0

    R00 = R00*np.asmatrix(np.array([[0,-1,0],[1,0,0],[0,0,1]])) # base
    R01 = R01 # s0
    R02 = R02 # s1
    R03 = R03 # e0
    R04 = R04 # e1
    R05 = R05 # w0
    R06 = R06 # w1
    R07 = R07 # w2
    
    #trans0 = Vector3(xyz0[0],xyz0[1],xyz0[2])
    trans1 = Vector3(xyz1[0],xyz1[1],xyz1[2])
    trans2 = Vector3(xyz2[0],xyz2[1],xyz2[2])
    trans3 = Vector3(xyz3[0],xyz3[1],xyz3[2])
    trans4 = Vector3(xyz4[0],xyz4[1],xyz4[2])
    trans5 = Vector3(xyz5[0],xyz5[1],xyz5[2])
    trans6 = Vector3(xyz6[0],xyz6[1],xyz6[2])
    trans7 = Vector3(xyz7[0],xyz7[1],xyz7[2])
    trans8 = Vector3(xyz8[0],xyz8[1],xyz8[2])
    trans8_2 = Vector3(xyz8_2[0],xyz8_2[1],xyz8_2[2])
    
    quat0 = quat_calc(R00)
    quat1 = quat_calc(R01)
    quat2 = quat_calc(R02)
    quat3 = quat_calc(R03)
    quat4 = quat_calc(R04)
    quat5 = quat_calc(R05)
    quat6 = quat_calc(R06)
    quat7 = quat_calc(R07)

    transforms.right.to_s0.rotation = quat1
    transforms.right.to_s0.translation = trans1
    transforms.right.to_s1.rotation = quat2
    transforms.right.to_s1.translation = trans2
    transforms.right.to_e0.rotation = quat3
    transforms.right.to_e0.translation = trans3
    transforms.right.to_e1.rotation = quat4
    transforms.right.to_e1.translation = trans4
    transforms.right.to_w0.rotation = quat5
    transforms.right.to_w0.translation = trans5
    transforms.right.to_w1.rotation = quat6
    transforms.right.to_w1.translation = trans6
    transforms.right.to_w2.rotation = quat7
    transforms.right.to_w2.translation = trans7
    transforms.right.to_gripper1.rotation = quat7
    transforms.right.to_gripper1.translation = trans8
    transforms.right.to_gripper2.rotation = quat7
    transforms.right.to_gripper2.translation = trans8_2

    return transforms

def run_server():
    rospy.init_node('find_baxter_transform_server')
    s = rospy.Service('find_baxter_transform', FindBaxterTransform, handle_find_transform)
    print 'Ready to use find_baxter_transform'
    rospy.spin()

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'local':
        rospy.init_node('find_baxter_transform_server')
        class Req(object):
            def __init__(self):
                self.config = BaxterConfiguration()
        print handle_find_transform(Req())
    else:
        run_server()

