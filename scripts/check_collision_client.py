#!/usr/bin/env python

import os
import sys
import rospy
from drawer_conquer.srv import \
    AddModel, \
    CheckCollision, \
    LoadModel
from drawer_conquer.msg import \
    BaxterConfiguration, \
    NamedTransform, \
    Model3D
                               

def run_add_model():
    model = Model3D()
    service_name = 'add_model'
    rospy.wait_for_service(service_name)
    try:
        add_model = rospy.ServiceProxy(service_name, AddModel)
        response = add_model('dummy-model-1', model)
        print 'Response:', response
    except rospy.ServiceException, e:
        print 'Service call add_model failed:', e

def run_load_model():
    scriptdir = os.path.abspath(os.path.dirname(__file__))
    modeldir = os.path.join(scriptdir, '..', 'models')
    modeldir = os.path.abspath(modeldir)
    modelfile = os.path.join(modeldir, 'table.stl')
    service_name = 'load_model'
    rospy.wait_for_service(service_name)
    try:
        load_model = rospy.ServiceProxy(service_name, LoadModel)
        response = load_model('table', modelfile)
        print 'Request: ', ('table', modelfile)
        print 'Response:', response
    except rospy.ServiceException, e:
        print 'Service call load_model failed:', e

def run_check_collision():
    model_transforms = [NamedTransform()]
    model_transforms[0].name = 'dummy-model-1'
    model_transforms.append(model_transforms[0])
    config = BaxterConfiguration()
    service_name = 'check_collision'
    rospy.wait_for_service(service_name)
    try:
        check_collision = rospy.ServiceProxy(service_name, CheckCollision)
        response = check_collision(model_transforms, config)
        print 'Response:', response
    except rospy.ServiceException, e:
        print 'Service call check_collision failed:', e

def main(arguments):
    run_add_model()
    run_load_model()
    run_check_collision()

if __name__ == '__main__':
    main(sys.argv[1:])
