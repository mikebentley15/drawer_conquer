#!/usr/bin/env python
import ConfigParser
import baxter_interface
import numpy as np
import pickle
import rospy
import sys
import tf
import math
import random
from baxter_pykdl import baxter_kinematics
from baxter_constraints import BaxterDOFConstraints
from copy import copy
from geometry_msgs.msg import Point, Quaternion, Pose, PoseStamped
from ik_solver import InverseKinematics
from fk_solver import ForwardKinematics
from rrt import RRT, PoseDetector, RosCollision
from trajectory_util import Trajectory, JointPlayer
from trajectory_util import util as traj_util

s = np.sin
c = np.cos
pi = np.pi

_RRT = 'rrt'
_RRT_CONNECT = 'rrt_connect'
_BIRRT_CONNECT = 'birrt_connect'


class BaxterRRT:
    def __init__(self,
            limb,
            algorithm=_BIRRT_CONNECT,
            num_samples=1000,
            step_length=0.1,
            connect_prob=0.05,
            collision_func=None,
            baxter_constraints=None,
            filename=None,
            ):
        rospy.init_node('baxter_rrt')
        self.limb = limb
        # initialize the arm
        self.arm = traj_util.init_limb(limb)
        self.joint_names = self.arm.joint_names()
        self.lims = np.array([
            [-1.7016, 1.7016],  # s0
            [-2.1470, 1.0470],  # s1
            [-3.0541, 3.0541],  # e0
            [-0.0500, 2.6180],  # e1
            [-3.0590, 3.0590],  # w0
            [-1.5707, 2.0940],  # w1
            [-3.0590, 3.0590]   # w2
        ])

        # initialize the gripper
        self.gripper = baxter_interface.Gripper(limb)
        #self.gripper.calibrate()

        self.algorithm = algorithm
        self.num_samples = num_samples
        self.step_length = step_length
        self.connect_prob = connect_prob
        self.collision_func = collision_func
        if collision_func is None:
            self.collision_func = lambda x: False
        self.filename = filename

        self.baxter_constraints = baxter_constraints
        if baxter_constraints is not None:
            self.constraint_error_func = self.baxter_constraints.get_ts_error
            self.jacobian_inv_func = self.baxter_constraints.jacobian_pseudo_inverse

        self.rrt = RRT(
            num_samples=self.num_samples,
            step_length=self.step_length,
            lims=self.lims,
            connect_prob=self.connect_prob,
            collision_func=self.collision_func,
            constraint_error_func=self.constraint_error_func,
            jacobian_inv_func=self.jacobian_inv_func,
        )
        self.brick_detector = PoseDetector('brick')
        self.drawer_inner_detector = PoseDetector('drawer_inner')
        self.drawer_outer_detector = PoseDetector('drawer_outer')
        self.brick_detector.subscribe('/brick_ar_tag/pose')
        self.drawer_inner_detector.subscribe('/drawer_inner_ar_tag/pose')
        self.drawer_outer_detector.subscribe('/drawer_outer_ar_tag/pose')

    def get_plan(self, init, goal):
        'Runs the planning algorithm and returns a plan or None if no plan was found'
        # Early check to make sure that we are not in collision
        if self.collision_func(goal):
            print 'RRT goal is in collision!'
            return None
        if self.collision_func(init):
            print 'RRT init is in collision!'
            return None

        # Try a straight line
        plan = self.rrt.build_straight_line_path(init, goal)
        if plan is not None:
            print 'Found a straight line path.  Skipping RRT'
            return plan
        else:
            print 'Straight line path is in collision.'

        if self.algorithm == _RRT:
            print 'Running RRT...'
            plan = self.rrt.build_rrt(init, goal)
        elif self.algorithm == _RRT_CONNECT:
            print 'Running RRT Connect...'
            plan = self.rrt.build_rrt_connect(init, goal)
        elif self.algorithm == _BIRRT_CONNECT:
            print 'Running BiRRT Connect...'
            plan = self.rrt.build_rrt_connect_bidirectional(init, goal)
        else:
            print 'Algorithm unknown:', self.algorithm
            sys.exit(1)
        return plan

    def post_rrt_smooth(self, path):
        'Smooths the path after the RRT'
        # Try to take shortcuts
        path = copy(path)
        trial_count = len(path)/2
        for _ in range(trial_count):
            samples = random.sample(path, 2)
            idx_1 = path.index(samples[0])
            idx_2 = path.index(samples[1])
            if idx_1 > idx_2:
                samples.reverse()
                idx_1, idx_2 = idx_2, idx_1
            inner_path = self.rrt.build_straight_line_path(init, goal)
            if inner_path is not None:
                path = path[0:idx_1] + inner_path + path[idx_2+1:]
        # TODO: spline paths
        return path

    def get_trajectory(self, plan, duration=5.0):
        trajectory = Trajectory(self.joint_names)
        ts = duration / float(len(plan))
        time = 0
        for i, position in enumerate(plan):
            pos_dict = dict(zip(self.joint_names, position))
            trajectory.add_position(pos_dict)
            # add extra time to last position for move to goal to reduce speed
            if i == len(plan) - 1:
                time += 0.5
            trajectory.add_timestep(time)
            time += ts
        return trajectory

    def execute_trajectory(self, trajectory):
        print 'Executing trajectory...'
        jp = JointPlayer(trajectory, self.limb) # make sure JTAS is running first
        jp.start()
        result = jp.wait()
        if result:
            print 'Trajectory executed successfully.'
        else:
            print 'Trajectory execution failed.'

    def get_current_position(self):
        current_angles = self.arm.joint_angles()
        pos = [current_angles[joint_name] for joint_name in self.joint_names]
        return pos

    def get_above_object_pose(self, obj_pose, dist_above=0.1):
        obj_q = obj_pose.orientation
        tf_euler = tf.transformations.euler_from_quaternion((obj_q.x, obj_q.y,
            obj_q.z, obj_q.w))
        tf_euler_mat = tf.transformations.euler_matrix(tf_euler[0],
            tf_euler[1], tf_euler[2]) # Rot of obj w.r.t. torso
        tf_euler_mat[0, 0] = -tf_euler_mat[0, 0]
        tf_euler_mat[1, 0] = -tf_euler_mat[1, 0]
        tf_euler_mat[2, 2] = -tf_euler_mat[2, 2]
        T1 = np.array([[0, -1, 0, 0],
                       [1, 0, 0, 0],
                       [0, 0, 1, 0],
                       [0, 0, 0, 1]])
        tf_euler_mat = np.dot(T1, tf_euler_mat)
        q = tf.transformations.quaternion_from_matrix(tf_euler_mat)
        gripper_q = Quaternion(q[0], q[1], q[2], q[3])
        gripper_p = obj_pose.position
        gripper_p.z += dist_above
        gripper_pose = Pose(gripper_p, gripper_q)
        return gripper_pose

    def get_at_drawer_pose(self, drawer_pose, theta=0.6, dist_in_front=0.0, dist_below=0.0):
        """ Theta is angle from vertical in the 'x' direction from torso. """
        qd = copy(drawer_pose.orientation)
        qd = np.array([qd.x, qd.y, qd.z, qd.w])
        qx = np.array([1, 0, 0, 0])                     # rotate about x by pi
        qz = np.array([0, 0, s(pi/4), c(pi/4)])         # rotate about z by pi/2
        qx2 = np.array([-s(theta/2), 0, 0, c(theta/2)]) # rotate about x by theta
        qz2 = np.array([0, 0, s(pi/16), c(pi/16)])
        q = tf.transformations.quaternion_multiply(qd, qx)
        q = tf.transformations.quaternion_multiply(q, qz)
        q = tf.transformations.quaternion_multiply(q, qx2)
        #q = tf.transformations.quaternion_multiply(q, qz2)

        R = tf.transformations.quaternion_matrix(q)
        pos = R.dot([0, 0.046, -0.075, 1])
        #print 'pos:', pos

        R_drawer = tf.transformations.quaternion_matrix(qd)
        #print 'R_drawer:', R_drawer
        shift = R_drawer.dot([dist_below, 0, dist_in_front, 1])
        #print 'shift:', shift

        gripper_q = Quaternion(q[0], q[1], q[2], q[3])
        gripper_p = copy(drawer_pose.position)
        gripper_p.x += pos[0] + shift[0]
        gripper_p.y += pos[1] + shift[1]
        gripper_p.z += pos[2] + shift[2]
        gripper_pose = Pose(gripper_p, gripper_q)
        return gripper_pose

    def save_trajectory(self, trajectory):
        with open(self.filename + '.trajectory', 'w') as f:
            pickle.dump(trajectory, f, -1)

    def save_plan(self, plan):
        with open(self.filename + '.plan', 'w') as f:
            pickle.dump(plan, f, -1)

    def load_trajectory(self, filename):
        with open(filename, 'r') as f:
            trajectory = pickle.load(f)
        return trajectory

    def load_plan(self, filename):
        with open(filename, 'r') as f:
            plan = pickle.load(f)
        return plan

    def path_to_pose(self, from_pose, to_pose):
        print '\nRunning IK to find goal state...'
        ik = InverseKinematics()
        soln_dict = ik.ik_request(from_pose, ik.left_iksvc)
        print 'ik_request({0}, {1}) -> {2}'.format(from_pose, ik.left_iksvc, soln_dict)
        if soln_dict:
            goal = np.array([soln_dict[joint_name] for joint_name in self.joint_names])
            plan = self.get_plan(init, goal)
            self.save_plan(plan)
            if plan is not None:
                return plan
            else:
                print 'No plan could be found.'
                raise RuntimeError('No plan could be found. ({0}, {1}, {2})'.format(theta, dist_in_front, dist_below))

        else:
            raise RuntimeError('No Valid Joint Solution Found: ({0}, {1}, {2})'.format(theta, dist_in_front, dist_below))
        

    def path_to_drawer_pose(self, init, theta, dist_in_front, dist_below):
        drawer_p = self.drawer_inner_detector.position
        drawer_q = self.drawer_inner_detector.quaternion
        drawer_pose = Pose(drawer_p, drawer_q)
        gripper_pose = self.get_at_drawer_pose(drawer_pose, theta=theta, dist_in_front=dist_in_front, dist_below=dist_below)
        print 'GRIPPER POSE\n', gripper_pose
        return self.path_to_pose(init, gripper_pose)

def _angles_exceeded(angles):
    abs_values = [abs(angle) for angle in angles]
    exceeded = any([val > 3.1 for val in abs_values])
    if exceeded:
        raise ValueError('Angles are too big! You might be near a singularity.')

def open_and_close_drawer_hardcoded():
    print '\nInitializing Baxter RRT node...'
    # parameters for RRT execution
    limb = 'left'
    algorithm = _RRT
    num_samples = 2000
    step_length = 0.1
    connect_prob = 0.25
    collision_func = None # never in collision for now
    # will save path to FILENAME.path and trajectory to FILENAME.trajectory
    filename = 'rrt_move_to_object'

    baxter_rrt = BaxterRRT(
        limb=limb,
        algorithm=algorithm,
        num_samples=num_samples,
        step_length=step_length,
        connect_prob=connect_prob,
        collision_func=collision_func,
        filename=filename,
    )

    rc = RosCollision()
    rc.set_current_arm(limb)
    rc.set_important_models(['drawer_outside', 'brick'])
    rc.add_pose_detector('drawer_outside', baxter_rrt.drawer_outer_detector)
    rc.add_pose_detector('brick', baxter_rrt.brick_detector)
    # TODO: turn on collision checking again later
    #baxter_rrt.collision_func = rc.check_collision
    #baxter_rrt.rrt.in_collision = rc.check_collision

    print 'Initialization complete.'

    if baxter_rrt.drawer_inner_detector.has_value:
        print 'Object pose known.'
        dist_down = 0.05

        init = np.asarray(baxter_rrt.get_current_position())

        dist_in_front = 0.0
        dist_below = 0.0
        path = baxter_rrt.path_to_drawer_pose(init, pi/6, 0.011, 0.030)
        path.extend(baxter_rrt.path_to_drawer_pose(path[-1], pi/5, 0.050, 0.005))
        path.extend(baxter_rrt.path_to_drawer_pose(path[-1], pi/5, 0.150,-0.005))
        path.extend(baxter_rrt.path_to_drawer_pose(path[-1], pi/3, 0.350,-0.015))
        path.extend(baxter_rrt.path_to_drawer_pose(path[-1], pi/6, 0.005, 0.005))

        step_duration = 0.027 / step_length # seconds / radian
        trajectory = baxter_rrt.get_trajectory(path, 0.2 + step_duration * len(path))
        baxter_rrt.save_trajectory(trajectory)
        baxter_rrt.execute_trajectory(trajectory)

        print 'path:', path

def open_and_close_drawer_manifold():
    print '\nInitializing Baxter RRT node...'
    # parameters for RRT execution
    limb = 'left'
    algorithm = _RRT
    num_samples = 2000
    step_length = 0.1
    connect_prob = 0.25
    collision_func = None # never in collision for now
    constraints = np.array([0, 1, 1, 1, 1, 1])
    # will save path to FILENAME.path and trajectory to FILENAME.trajectory
    filename = 'rrt_move_to_object'

    baxter_rrt = BaxterRRT(
        limb=limb,
        algorithm=algorithm,
        num_samples=num_samples,
        step_length=step_length,
        connect_prob=connect_prob,
        collision_func=collision_func,
        #baxter_constraints=BaxterDOFConstraints(limb, constraints),
        filename=filename,
    )

    rc = RosCollision()
    rc.set_current_arm(limb)
    rc.set_important_models(['drawer_outside', 'brick'])
    rc.add_pose_detector('drawer_outside', baxter_rrt.drawer_outer_detector)
    rc.add_pose_detector('brick', baxter_rrt.brick_detector)
    # TODO: turn on collision checking again later
    #baxter_rrt.collision_func = rc.check_collision
    #baxter_rrt.rrt.in_collision = rc.check_collision

    print 'Initialization complete.'

    if baxter_rrt.drawer_inner_detector.has_value:
        print 'Object pose known.'
        dist_down = 0.05

        init = np.asarray(baxter_rrt.get_current_position())

        dist_in_front = 0.0
        dist_below = 0.0
        path = baxter_rrt.path_to_drawer_pose(init, pi/6, 0.011, 0.030)
        path.extend(baxter_rrt.path_to_drawer_pose(path[-1], pi/5, 0.050, 0.005))

        # Add drawer constraints
        baxter_rrt.baxter_constraints = BaxterDOFConstraints(limb, constraints)
        baxter_rrt.jacobian_inv_func = baxter_rrt.baxter_constraints.jacobian_pseudo_inverse
        baxter_rrt.constraint_err_func = baxter_rrt.baxter_constraints.get_ts_error
        
        # Solve for opening drawer with constraints
        fk = ForwardKinematics(limb)
        at_drawer = fk.forward_position_kinematics(zip(fk._joint_names, path[-1]))
        open_pose = copy(at_drawer)
        open_pose[0] += 0.35
        path.extend(baxter_rrt.path_to_pose(path[-1], open_pose)
        ik = InverseKinematics(limb)
        open_joints_dict = ik.ik_request(poen_pose, ik.left_iksvc)
        if open_joints_dict:
            goal = np.array([open_joints_dict[name] for name in fk._joint_names])
            
        # Remove the constraints again
        baxter_rrt.rrt.jacobian_inv_func = None
        baxter_rrt.rrt.constraint_error_func = None

        opening_path = solve(open_pose)
        path.extend(opening_path)

        # Close is the reverse of opening
        path.extend(reversed(opening_path))
 
        # Now run the path
        step_duration = 0.027 / step_length # seconds / radian
        trajectory = baxter_rrt.get_trajectory(path, 0.2 + step_duration * len(path))
        baxter_rrt.save_trajectory(trajectory)
        baxter_rrt.execute_trajectory(trajectory)

        print 'path:', path

def main():
    open_and_close_drawer_hardcoded()

        #print 'GRIPPER POSE\n', gripper_pose

        #print '\nRunning IK to find goal state...'
        #ik = InverseKinematics()
        #soln_dict = ik.ik_request(gripper_pose, ik.left_iksvc)
        #if soln_dict:
        #    goal = np.array([soln_dict[joint_name] for joint_name in baxter_rrt.joint_names])
        #    plan = baxter_rrt.get_plan(init, goal)
        #    baxter_rrt.save_plan(plan)
        #    if plan is not None:
        #        print 'RRT plan found.  # steps: ', len(plan)
        #        print plan
        #        trajectory = baxter_rrt.get_trajectory(plan, 0.2 + step_duration * len(plan))
        #        baxter_rrt.save_trajectory(trajectory)
        #        baxter_rrt.execute_trajectory(trajectory)
        #    else:
        #        print 'No plan could be found.'

    # ================================ CAN DELETE THIS SECTION ============================================

    # Adam added this to show constraints are working for RRT, kill it if you want

    # ik = InverseKinematics()
    # p = [
    #     0.93665535,
    #     0.13919551,
    #     0.37981012,
    #     -0.16295176,
    #     0.15880441,
    #     -0.65194067,
    #     0.72332652
    # ]
    # init_pose = Pose(Point(p[0], p[1], p[2]), Quaternion(p[3], p[4], p[5], p[6]))
    # soln_dict = ik.ik_request(init_pose, ik.left_iksvc)
    # if soln_dict is not None:
    #     baxter_rrt.arm.move_to_joint_positions(soln_dict)
    #     init = np.array([soln_dict[joint_name] for joint_name in baxter_rrt.joint_names])
    #     goal_pose = init_pose
    #     goal_pose.position.x -= 0.3
    #     soln_dict = ik.ik_request(goal_pose, ik.left_iksvc)
    #     if goal_pose is not None:
    #         goal = np.array([soln_dict[joint_name] for joint_name in baxter_rrt.joint_names])
    #         plan = baxter_rrt.get_plan(init, goal)
    #         if plan is not None:
    #             print '\nPLAN:\n'
    #             print plan
    #             trajectory = baxter_rrt.get_trajectory(plan, 7.0)
    #             baxter_rrt.execute_trajectory(trajectory)

# ======================================================================================================





if __name__ == '__main__':
    main()
