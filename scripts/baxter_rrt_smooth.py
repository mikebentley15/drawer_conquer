#!/usr/bin/env python
import ConfigParser
import baxter_interface
import numpy as np
import pickle
import rospy
import sys
import tf
import math
from baxter_pykdl import baxter_kinematics
from copy import copy
from geometry_msgs.msg import Point, Quaternion, Pose, PoseStamped
from ik_solver import InverseKinematics
from rrt import RRT, PoseDetector, RosCollision
from trajectory_util import Trajectory, JointPlayer
from trajectory_util import util as traj_util
import random as rd


_RRT = 'rrt'
_RRT_CONNECT = 'rrt_connect'
_BIRRT_CONNECT = 'birrt_connect'


class BaxterRRT:
    def __init__(self, limb, algorithm=_BIRRT_CONNECT, num_samples=1000,
                 step_length=0.1, connect_prob=0.05, collision_func=None,
                 trajectory_duration=10.0, filename=None):
        rospy.init_node('baxter_rrt')
        self.limb = limb
        # initialize the arm
        self.arm = traj_util.init_limb(limb)
        self.joint_names = self.arm.joint_names()
        self.lims = np.array([
            [-1.7016, 1.7016],  # s0
            [-2.147, 1.047],    # s1
            [-3.0541, 3.0541],  # e0
            [-0.05, 2.618],     # e1
            [-3.059, 3.059],    # w0
            [-1.5707, 2.094],   # w1
            [-3.059, 3.059]     # w2
        ])

        # initialize the gripper
        self.gripper = baxter_interface.Gripper(limb)
        self.gripper.calibrate()

        self.algorithm = algorithm
        self.num_samples = num_samples
        self.step_length = step_length
        self.connect_prob = connect_prob
        self.collision_func = collision_func
        self.trajectory_duration = trajectory_duration
        self.filename = filename
        self.rrt = RRT(
            num_samples=self.num_samples, 
            step_length=self.step_length, 
            lims=self.lims, 
            connect_prob=self.connect_prob, 
            collision_func=self.collision_func
        )
        # rospy.Subscriber('/brick_ar_tag/pose', PoseStamped,
        #     self.register_object_pose)
        # keep lists of recorded positions/orientations to get average over time
        self.brick_detector = PoseDetector('brick')
        self.drawer_inner_detector = PoseDetector('drawer_inner')
        self.drawer_outer_detector = PoseDetector('drawer_outer')
        self.brick_detector.subscribe('/brick_ar_tag/pose')
        self.drawer_inner_detector.subscribe('/drawer_inner_ar_tag/pose')
        self.drawer_outer_detector.subscribe('/drawer_outer_ar_tag/pose')

    #def register_object_pose(self, ps_msg):
    #    if self.object_pose is None:
    #        # get 10 poses to average over
    #        if len(self.positions) < 20:
    #            p = ps_msg.pose.position
    #            o = ps_msg.pose.orientation
    #            self.positions.append((p.x, p.y, p.z))
    #            self.orientations.append((o.x, o.y, o.z, o.w))
    #        else:
    #            print 'Object pose found.'
    #            # get average pose over 10 recorded
    #            self.positions = np.array(self.positions)
    #            self.orientations = np.array(self.orientations)
    #            p = np.mean(self.positions, axis=0)
    #            o = np.mean(self.orientations, axis=0)
    #            self.position = Point(x=p[0], y=p[1], z=p[2])
    #            self.quaternion = Quaternion(x=o[0], y=o[1], z=o[2], w=o[3])
    #            pose = Pose(position=self.position, orientation=self.quaternion)
    #            # so you can listen to tag close and then move to more
    #            # interesting position
#   #             self.arm.move_to_neutral()
    #            self.object_pose = pose

                
    def get_plan(self, init, goal):
        if self.algorithm == _RRT:
            print '\nRunning RRT...'
            plan = self.rrt.build_rrt(init, goal)
        elif self.algorithm == _RRT_CONNECT:
            print '\nRunning RRT Connect...'
            plan = self.rrt.build_rrt_connect(init, goal)
        elif self.algorithm == _BIRRT_CONNECT:
            print '\nRunning BiRRT Connect...'
            plan = self.rrt.build_rrt_connect_bidirectional(init, goal)
        else:
            print '\nAlgorithm unknown:', self.algorithm
            sys.exit(1)
        return plan

    def collision_for_smooth(self,path,i,j):
         if j == i + 1:
             return False
         else:
             return self.rrt.build_straight_line_path2(path[i], path[j])

    def post_rrt_smooth(self, path,collision_func):
        reach_goal = False
        i = 0
        smooth_path = []
        smooth_path.append(path[0])
        j = i+1
        while  i <= len(path)-1:
           if i <= len(path)-3:
              j = i +1
              while j <= len(path) - 1 and j-i <=4:
                 judge = collision_func(path,i,j)
                 if judge == False and j - i < 4:
                     j = j +1
                     continue

                 elif judge == False and j - i == 4:
                     smooth_path.append(path[j-1])
                     i = j -1
                     break
                 elif judge == True:
                     smooth_path.append(path[j-1])
                     i = j -1
                     break
           elif i == len(path)-2 :
              #smooth_path.append(path[i])
              smooth_path.append(path[i+1])
              break
           elif i == len(path) -1:
              smooth_path.append(path[i])
              break

        #smooth_path.append(path[i-1])
        return smooth_path

    def post_rrt_smooth2(self, path,collision_func):
        reach_goal = False
        i = 0
        smooth_path = []
        smooth_path.append((path[0],2))
        j = i+1
        while  i <= len(path)-1:
           if i <= len(path)-3:
              j = i +1
              while j <= len(path) - 1 :
                 judge = collision_func(path,i,j)
                 if judge == False:
                     j = j +1
                     continue

                 elif judge == True:
                     smooth_path.append((path[j-1],j-i-1))
                     i = j -1
                     break
           elif i == len(path)-2 :
              #smooth_path.append(path[i])
              smooth_path.append((path[i+1],1))
              break
           elif i == len(path) -1:
              smooth_path.append((path[i],1))
              break

        #smooth_path.append(path[i-1])
        return smooth_path

    def get_trajectory2(self, plan, duration=5.0):
        trajectory = Trajectory(self.joint_names)
        ts = duration / float(len(plan))
        time = 0
        for i, position in enumerate(plan):
            pos_dict = dict(zip(self.joint_names, position[0]))
            trajectory.add_position(pos_dict)
            # add extra time to last position for move to goal to reduce speed
            if i == len(plan) - 1:
                time += 0.5*position[1]
            trajectory.add_timestep(time)
            time += ts
        return trajectory

    def fake_in_collision(self,path, i, j):
        '''
        We never collide with this function!
        '''
        if j == i + 1:
            return False
        else:
            judge = [True, False, False, False, False, False,False]
            return rd.choice(judge)

    def get_trajectory(self, plan, duration=5.0):
        trajectory = Trajectory(self.joint_names)
        ts = duration / float(len(plan))
        time = 0
        for i, position in enumerate(plan):
            pos_dict = dict(zip(self.joint_names, position))
            trajectory.add_position(pos_dict)
            # add extra time to last position for move to goal to reduce speed
            if i == len(plan) - 1:
                time += 0.5
            trajectory.add_timestep(time)
            time += ts
        return trajectory
        
    def execute_trajectory(self, trajectory):
        print 'Executing trajectory...'
        jp = JointPlayer(trajectory, self.limb) # make sure JTAS is running first
        jp.start()
        result = jp.wait()
        if result:
            print 'Trajectory executed successfully.'
        else:
            print 'Trajectory execution failed.'

    def get_current_position(self):
        current_angles = self.arm.joint_angles()
        pos = [current_angles[joint_name] for joint_name in self.joint_names]
        return pos

    def get_above_object_pose(self, obj_pose, dist_above=0.1):
        obj_q = obj_pose.orientation
        tf_euler = tf.transformations.euler_from_quaternion((obj_q.x, obj_q.y,
            obj_q.z, obj_q.w))
        tf_euler_mat = tf.transformations.euler_matrix(tf_euler[0],
            tf_euler[1], tf_euler[2]) # Rot of obj w.r.t. torso
        tf_euler_mat[0, 0] = -tf_euler_mat[0, 0]
        tf_euler_mat[1, 0] = -tf_euler_mat[1, 0]
        tf_euler_mat[2, 2] = -tf_euler_mat[2, 2]        
        T1 = np.array([[0, -1, 0, 0], 
                       [1, 0, 0, 0], 
                       [0, 0, 1, 0], 
                       [0, 0, 0, 1]])
        tf_euler_mat = np.dot(T1, tf_euler_mat)
        q = tf.transformations.quaternion_from_matrix(tf_euler_mat)
        gripper_q = Quaternion(q[0], q[1], q[2], q[3])
        gripper_p = obj_pose.position
        gripper_p.z += dist_above
        gripper_pose = Pose(gripper_p, gripper_q)
        return gripper_pose

    def get_at_drawer_pose(self, drawer_pose, theta=0.6, dist_in_front=0.2, dist_below=0.1):
        """ Theta is angle from vertical in the 'x' direction from torso. """
        T = np.array([[0, np.cos(theta), np.sin(theta), 0], 
                      [-1, 0, 0, 0], 
                      [0, -np.sin(theta), np.cos(theta), 0], 
                      [0, 0, 0, 1]])
        q = tf.transformations.quaternion_from_matrix(T)
        gripper_q = Quaternion(q[0], q[1], q[2], q[3])
        gripper_p = drawer_pose.position
        gripper_p.x -= dist_in_front
        gripper_p.z -= dist_below
        gripper_pose = Pose(gripper_p, gripper_q)
        return gripper_pose

    def ik_search(self, pose, num_attempts=10000):
        p = pose.position
        q = pose.orientation
        bk = baxter_kinematics('left')
        angles = None
        
        count = 0
        while angles is None and count < num_attempts:
            seed = self.get_random_joint_angles()
            angles = bk.inverse_kinematics((p.x, p.y, p.z), (q.x, q.y, q.z, q.w), seed)
            count += 1
        
        if angles is not None:
            print 'SOLUTION FOUND'
            print angles
        else:
            print 'No IK solution found.'

    def get_random_joint_angles(self):
        return [np.random.choice(np.linspace(joint_lims[0], joint_lims[1], 100)) for joint_lims in self.lims]
            
    def save_trajectory(self, trajectory):
        with open(self.filename + '.trajectory', 'w') as f:
            pickle.dump(trajectory, f, -1)

    def save_plan(self, plan):
        with open(self.filename + '.plan', 'w') as f:
            pickle.dump(plan, f, -1)

    def load_trajectory(self, filename):
        with open(filename, 'r') as f:
            trajectory = pickle.load(f)
        return trajectory

    def load_plan(self, filename):
        with open(filename, 'r') as f:
            plan = pickle.load(f)
        print 'PLAN IN FUNCTION', plan
        return plan

def _angles_exceeded(angles):
    abs_values = [abs(angle) for angle in angles]
    exceeded = any([val > 3.1 for val in abs_values])
    if exceeded:
        raise ValueError('Angles are too big! You might be near a singularity.')
        

def main():
    print '\nInitializing Baxter RRT node...'
    # parameters for RRT execution
    limb = 'left'
    algorithm = _BIRRT_CONNECT
    num_samples = 2000
    step_length = 0.2
    connect_prob = 0.25
    collision_func = None # never in collision for now
    trajectory_duration = 7.0 # seconds
    # will save path to FILENAME.path and trajectory to FILENAME.trajectory
    filename = 'rrt_move_to_object'
    
    baxter_rrt = BaxterRRT(
        limb=limb, 
        algorithm=algorithm, 
        num_samples=num_samples, 
        step_length=step_length, 
        connect_prob=connect_prob, 
        collision_func=collision_func, 
        trajectory_duration=trajectory_duration, 
        filename=filename
    )

    rc = RosCollision()
    rc.set_current_arm('left')
    rc.set_important_models(['drawer_outside'])
    rc.add_pose_detector('drawer_outside', baxter_rrt.drawer_outer_detector)
    baxter_rrt.collision_func = rc.check_collision
    baxter_rrt.rrt.in_collision = rc.check_collision

    trajectory_filename = 'rrt_move_to_object.trajectory'
    plan_filename = 'data.pkl'

    #trajectory = baxter_rrt.load_trajectory(trajectory_filename)
    plan = baxter_rrt.load_plan(plan_filename) # list of joint positions
    #print plan
    plan = []

    plan.append([1.2068593834899903,-1.0055244053100587,-1.1780972437500001,1.4933302953002932,0.5483981310424805,1.1604564647094728,0.8666991441650391])
    plan.append([ 1.13572134, -0.90127663, -1.17998809,  1.48235176,  0.42068945,
         1.18225563,  0.95133345])
    plan.append([ 1.0645833 , -0.79702885, -1.18187894,  1.47137322,  0.29298077,
         1.20405479,  1.03596776])
    plan.append([ 0.93985295, -0.75636621, -1.24570581,  1.51150591,  0.17903623,
         1.22188177,  0.97429742])
    plan.append([ 0.98729505, -0.66862078, -1.32225676,  1.41710497,  0.15149283,
         1.10899779,  0.93214332])
    plan.append([ 0.97401818, -0.70850984, -1.35500949,  1.46085236,  0.00578986,
         1.01265281,  1.00094958])
    plan.append([ 0.90435948, -0.60276572, -1.34270723,  1.44981562, -0.10893261,
         1.05216759,  1.09565981])
    plan.append([ 0.83470078, -0.49702161, -1.33040498,  1.43877889, -0.22365509,
         1.09168237,  1.19037004])
    plan.append([ 0.74063393, -0.54665969, -1.40990613,  1.43759023, -0.36236284,
         1.05029441,  1.15278229])
    plan.append([ 0.67891732, -0.43340105, -1.38923157,  1.42641698, -0.4656542 ,
         1.09494001,  1.25352416])
    plan.append([ 0.61720071, -0.32014241, -1.36855701,  1.41524372, -0.56894556,
         1.13958561,  1.35426604])
    plan.append([ 0.5554841 , -0.20688376, -1.34788245,  1.40407046, -0.67223692,
         1.18423121,  1.45500792])
    plan.append([ 0.57204868, -0.22125816, -1.17564205,  1.4025692 , -0.75031622,
         1.16503892,  1.51319093])
    plan.append([ 0.50514054, -0.10091869, -1.18037134,  1.39114309, -0.84614356,
         1.21454074,  1.60939635])
    plan.append([ 0.4382324 ,  0.01942078, -1.18510062,  1.37971699, -0.9419709 ,
         1.26404255,  1.70560176])
    plan.append([ 0.42951024,  0.08392465, -1.3136155 ,  1.36906792, -1.00561165,
         1.16908983,  1.78349615])
    plan.append([ 0.35905304,  0.19901645, -1.28812275,  1.35921808, -1.09433542,
         1.24537821,  1.86923374])
    plan.append([ 0.28859584,  0.31410825, -1.26262999,  1.34936823, -1.1830592 ,
         1.32166659,  1.95497134])
    plan.append([ 0.21813864,  0.42920005, -1.23713724,  1.33951838, -1.27178297,
         1.39795497,  2.04070893])
    plan.append([ 0.14768144,  0.54429186, -1.21164449,  1.32966854, -1.36050675,
         1.47424334,  2.12644652])
    plan.append([ 0.13379296,  0.56697868, -1.20661937,  1.32772694, -1.37799593,
         1.48928126,  2.14334707])
    #print plan
    '''
    plan = []
    plan.append([1.2068593834899903, -1.0055244053100587, -1.1780972437500001, 1.4933302953002932, 0.5483981310424805,
                 1.1604564647094728, 0.8666991441650391])
    plan.append([0.93985295, -0.75636621, -1.24570581, 1.51150591, 0.17903623, 1.22188177, 0.97429742])
    plan.append([0.90435948, -0.60276572, -1.34270723, 1.44981562, -0.10893261, 1.05216759, 1.09565981])
    plan.append([0.67891732, -0.43340105, -1.38923157, 1.42641698, -0.4656542, 1.09494001, 1.25352416])
    plan.append([0.57204868, -0.22125816, -1.17564205, 1.4025692, -0.75031622, 1.16503892, 1.51319093])
    plan.append([0.42951024, 0.08392465, -1.3136155, 1.36906792, -1.00561165, 1.16908983, 1.78349615])
    plan.append([0.21813864, 0.42920005, -1.23713724, 1.33951838, -1.27178297, 1.39795497, 2.04070893])
    plan.append([0.14768144, 0.54429186, -1.21164449, 1.32966854, -1.36050675, 1.47424334, 2.12644652])
    plan.append([0.13379296, 0.56697868, -1.20661937, 1.32772694, -1.37799593, 1.48928126, 2.14334707])
    '''
    #print trajectory
    post_smooth_plan = baxter_rrt.post_rrt_smooth2(plan,baxter_rrt.fake_in_collision)
    print post_smooth_plan
    #print post_smooth_plan
    post_smooth_trajectory = baxter_rrt.get_trajectory2(post_smooth_plan,duration=9.0)
    baxter_rrt.execute_trajectory(post_smooth_trajectory)
    print post_smooth_plan

   #data1 = {'a': [1, 2.0, 3, 4 + 6j],
   #          'b': ('string', u'Unicode string'),
   #          'c': None}
    output = open('data.pkl', 'wb')
    pickle.dump(post_smooth_plan,output)
    output.close()






#     # get the current position
#     init = baxter_rrt.get_current_position()

#     print 'Initialization complete.'

#     #if baxter_rrt.brick_detector.has_value:
#     if baxter_rrt.drawer_inner_detector.has_value:
#         print 'Object pose known.'
#         dist_down = 0.05

#         drawer_p = baxter_rrt.drawer_inner_detector.position
#         drawer_q = baxter_rrt.drawer_inner_detector.quaternion
#         drawer_pose = Pose(drawer_p, drawer_q)
#         dist_in_front = 0.2
#         dist_below = 0.1
#         gripper_pose = baxter_rrt.get_at_drawer_pose(drawer_pose, dist_in_front=dist_in_front, dist_below=dist_below)
#         print 'GRIPPER POSE\n', gripper_pose

#         print '\nRunning IK to find goal state...'
#         ik = InverseKinematics()
# #        gripper_pose.position.x += dist_in_front
# #        gripper_pose.position.z += dist_below
#         soln_dict = ik.ik_request(gripper_pose, ik.left_iksvc)
#         if soln_dict:
#             goal = np.array([soln_dict[joint_name] for joint_name in baxter_rrt.joint_names])
#             plan = baxter_rrt.get_plan(init, goal)
#             baxter_rrt.save_plan(plan)
#             if plan is not None:
#                 print 'RRT plan found.'
#                 trajectory = baxter_rrt.get_trajectory(plan, trajectory_duration)
#                 baxter_rrt.save_trajectory(trajectory)
#                 baxter_rrt.execute_trajectory(trajectory)
#             else:
#                 print 'No plan could be found.'



        #             init = plan1[-1]
        #             goal = np.array([soln_dict[joint_name] for joint_name in baxter_rrt.joint_names])
        #             plan2 = baxter_rrt.get_plan(init, goal)
        #             plan = np.concatenate((plan1, plan2))
        #             trajectory = baxter_rrt.get_trajectory(plan, trajectory_duration)            
        #             baxter_rrt.execute_trajectory(trajectory)
        # else:
        #     print 'No IK solution found.'


#        baxter_rrt.ik_search(gripper_pose)



        # angles = bk.inverse_kinematics((p.x, p.y, p.z), (q.x, q.y, q.z, q.w))
        
        # if angles is not None: # and not _angles_exceeded(angles):
        #     goal = angles
        #     print 'Solution found:\n', goal
        # else:
        #     print 'No IK solution found.'

            # plan = baxter_rrt.get_plan(init, goal)
            # if plan is not None:
            #     trajectory = baxter_rrt.get_trajectory(plan, trajectory_duration)
            #    baxter_rrt.execute_trajectory(trajectory)




    #     # first get plan just above object
    #     brick_p = baxter_rrt.brick_detector.position
    #     brick_q = baxter_rrt.brick_detector.quaternion
    #     brick_pose = Pose(brick_p, brick_q)
    #     gripper_pose = baxter_rrt.get_above_object_pose(brick_pose)

    #     print '\nRunning IK to find goal state...'
    #     bk = baxter_kinematics('left')
    #     p = gripper_pose.position
    #     q = gripper_pose.orientation
    #     angles = bk.inverse_kinematics((p.x, p.y, p.z), (q.x, q.y, q.z, q.w))
        
    #     if angles is not None and not _angles_exceeded(angles):
    #         goal = angles
    #         print 'Solution found:', goal

    #         plan = baxter_rrt.get_plan(init, goal)
    #         if plan is not None:
    #             angles = bk.inverse_kinematics((p.x, p.y, p.z - dist_down), (q.x,
    #                                                                          q.y, q.z, q.w))
    #             if angles is not None and not _angles_exceeded(angles):
    #                 goal = angles
    #                 plan.append(goal)
    #                 trajectory = baxter_rrt.get_trajectory(plan, trajectory_duration)
    #                 baxter_rrt.execute_trajectory(trajectory)
    #                 rospy.sleep(0.5)
    #                 # close gripper
    #                 baxter_rrt.gripper.close()
    #                 rospy.sleep(0.5)
    #                 # plan to zero position
    #                 init = baxter_rrt.get_current_position()
    #                 goal = [0, 0, 0, 0, 0, 0, 0]
    #                 plan_to_zero = baxter_rrt.get_plan(init, goal)
    #                 trajectory = baxter_rrt.get_trajectory(plan_to_zero,
    #                                                        trajectory_duration)
    #                 baxter_rrt.execute_trajectory(trajectory)
    #             else:
    #                 raise ValueError('No IK solution found for pose at object.')
    #         else:
    #             print 'No path found.'
    #     else:
    #         raise ValueError('No IK solution found for pose above object.')
    # else:
    #     print '\nInitial object pose was unknown. Run scene calibration first.'

if __name__ == '__main__':
    main()
