#!/usr/bin/env python

import numpy as np
import rospy
from geometry_msgs.msg import PoseStamped

import ConfigParser
import os
import sys
import threading

mu = threading.Lock()
config = ConfigParser.SafeConfigParser()
section_title = 'SceneCalibration'
config.add_section(section_title)
objs_found = set()
all_objs = set([
    #'brick',
    'drawer_inner',
    'drawer_outer',
    ])

brick_poses = []
drawer_inner_poses = []
drawer_outer_poses = []
avg_count = 20

def register_pose(name, poses, ps_msg):
    if len(poses) < avg_count:
        print 'Received pose for', name
        p = ps_msg.pose.position
        o = ps_msg.pose.orientation
        poses.append((
            (p.x, p.y, p.z),
            (o.x, o.y, o.z, o.w),
            ))
        if len(poses) >= avg_count:
            print 'Object {0} pose found'.format(name)
            positions = np.asarray([x[0] for x in poses])
            orientations = np.asarray([x[1] for x in poses])
            pos = list(np.mean(positions, axis=0))
            orient = list(np.mean(orientations, axis=0))
            try:
                mu.acquire()
                objs_found.add(name)
                config.set(section_title, name + '.position.x', str(pos[0]))
                config.set(section_title, name + '.position.y', str(pos[1]))
                config.set(section_title, name + '.position.z', str(pos[2]))
                config.set(section_title, name + '.quaternion.x', str(orient[0]))
                config.set(section_title, name + '.quaternion.y', str(orient[1]))
                config.set(section_title, name + '.quaternion.z', str(orient[2]))
                config.set(section_title, name + '.quaternion.w', str(orient[3]))
                if all_objs.issubset(objs_found):
                    print 'All objects have been found'
                    rospy.signal_shutdown('job done')
            finally:
                mu.release()


def register_brick_pose(ps_msg):
    register_pose('brick', brick_poses, ps_msg)
def register_drawer_inner_pose(ps_msg):
    register_pose('drawer_inner', drawer_inner_poses, ps_msg)
def register_drawer_outer_pose(ps_msg):
    register_pose('drawer_outer', drawer_outer_poses, ps_msg)


def main(arguments):
    print 'Running calibration'
    calibration_filepath = os.path.join(os.path.abspath(os.path.join(os.curdir,
        'calibration.cfg')))
    print 'Will save to', calibration_filepath
    print '  Waiting to calibrate for the following objects:'
    print '    ' + '\n    '.join(sorted(all_objs))

    rospy.init_node('calibrate')
    s1 = rospy.Subscriber('/brick_ar_tag/pose', PoseStamped,
        register_brick_pose)
    s2 = rospy.Subscriber('/drawer_inner_ar_tag/pose', PoseStamped,
        register_drawer_inner_pose)
    s3 = rospy.Subscriber('/drawer_outer_ar_tag/pose', PoseStamped,
        register_drawer_outer_pose)

    while not rospy.is_shutdown():
        rospy.sleep(0.1)

    with open(calibration_filepath, 'wb') as configfile:
        print 'Saving to', calibration_filepath
        config.write(configfile)

if __name__ == '__main__':
    main(sys.argv[1:])
