#!/usr/bin/env python

import sys
import os
import stl

def main(arguments):
    for filename in arguments:
        print filename, '->',
        with open(filename, 'rb') as fin:
           obj = stl.read_binary_file(fin)

        # find set of vertices
        vertices = sorted(set([x for f in obj.facets for x in f.vertices]))
        new_vertices = []
        prev = vertices[0]
        vertex_map = {}
        
        # TODO: try to consolidate "close" vertices
        #for dim in range(3):
        #    vertices.sort(lambda x, y: x[dim] < y[dim])
        #    for v in vertices[1:]:
        #        if abs(sum([v[0]-prev[0], v[1]-prev[1], v[2]-prev[2]])) < 5e-13:
        #            try:
        #                vertices.remove(v)
        #                vertex_map[v] = prev
        #            except ValueError:
        #                pass
        #        else:
        #            prev = v

        # find triangles
        triangles = []
        for f in obj.facets:
            t = []
            triangles.append(t)
            for v in f.vertices:
                if v in vertex_map:
                    t.append(vertices.index(vertex_map[v])+1)
                else:
                    t.append(vertices.index(v)+1)

        filebase, _ = os.path.splitext(filename)
        outfilename = filebase + '.fcl'
        print outfilename
        with open(outfilename, 'w') as fout:
            for v in vertices:
                fout.write('v {0} {1} {2}\n'.format(v[0], v[1], v[2]))
            for t in triangles:
                fout.write('f {0} {1} {2}\n'.format(t[0], t[1], t[2]))

if __name__ == '__main__':
    main(sys.argv[1:])
