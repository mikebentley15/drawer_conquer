#!/usr/bin/python

from drawer_conquer.srv import FindBaxterTransform
from drawer_conquer.msg import BaxterTransforms
from geometry_msgs.msg import Transform, Vector3, Quaternion

import numpy as np
import PyKDL

import rospy

import baxter_interface

from baxter_kdl.kdl_parser import kdl_tree_from_urdf_model
from urdf_parser_py.urdf import URDF

class ForwardKinematics(object):
    """
    forward Kinematics with PyKDL
    """
    def __init__(self, limb):
        self._baxter = URDF.from_parameter_server(key='robot_description')
        self._kdl_tree = kdl_tree_from_urdf_model(self._baxter)
        self._base_link = self._baxter.get_root()
        self._tip_link = limb + '_gripper'
        self._tip_frame = PyKDL.Frame()
        self._arm_chain = self._kdl_tree.getChain(self._base_link,
                                                  self._tip_link)

        # Baxter Interface Limb Instances
        self._limb_interface = baxter_interface.Limb(limb)
        self._joint_names = self._limb_interface.joint_names()
        self._num_jnts = len(self._joint_names)

        self._base_to_s0_chain = PyKDL.Chain()
        self._base_to_s0_chain.addSegment(self._arm_chain.getSegment(0))
        self._base_to_s0_chain.addSegment(self._arm_chain.getSegment(1))
        self._base_to_s0_chain.addSegment(self._arm_chain.getSegment(2))
        self._s0_to_s1_chain = PyKDL.Chain(self._base_to_s0_chain)
        self._s0_to_s1_chain.addSegment(self._arm_chain.getSegment(3))
        self._s1_to_e0_chain = PyKDL.Chain(self._s0_to_s1_chain)
        self._s1_to_e0_chain.addSegment(self._arm_chain.getSegment(4))
        self._e0_to_e1_chain = PyKDL.Chain(self._s1_to_e0_chain)
        self._e0_to_e1_chain.addSegment(self._arm_chain.getSegment(5))
        self._e1_to_w0_chain = PyKDL.Chain(self._e0_to_e1_chain)
        self._e1_to_w0_chain.addSegment(self._arm_chain.getSegment(6))
        self._w0_to_w1_chain = PyKDL.Chain(self._e1_to_w0_chain)
        self._w0_to_w1_chain.addSegment(self._arm_chain.getSegment(7))
        self._w1_to_w2_chain = PyKDL.Chain(self._w0_to_w1_chain)
        self._w1_to_w2_chain.addSegment(self._arm_chain.getSegment(8))
        self._w2_to_gripper_chain = PyKDL.Chain(self._w1_to_w2_chain)
        self._w2_to_gripper_chain.addSegment(self._arm_chain.getSegment(9))
        self._w2_to_gripper_chain.addSegment(self._arm_chain.getSegment(10))
        self._w2_to_gripper_chain.addSegment(self._arm_chain.getSegment(11))

        # KDL Solvers
        self._fk_p_s0 = PyKDL.ChainFkSolverPos_recursive(self._base_to_s0_chain)
        self._fk_p_s1 = PyKDL.ChainFkSolverPos_recursive(self._s0_to_s1_chain)
        self._fk_p_e0 = PyKDL.ChainFkSolverPos_recursive(self._s1_to_e0_chain)
        self._fk_p_e1 = PyKDL.ChainFkSolverPos_recursive(self._e0_to_e1_chain)
        self._fk_p_w0 = PyKDL.ChainFkSolverPos_recursive(self._e1_to_w0_chain)
        self._fk_p_w1 = PyKDL.ChainFkSolverPos_recursive(self._w0_to_w1_chain)
        self._fk_p_w2 = PyKDL.ChainFkSolverPos_recursive(self._w1_to_w2_chain)
        self._fk_p_gripper = PyKDL.ChainFkSolverPos_recursive(self._w2_to_gripper_chain)
        self._chain_pieces = [
            self._fk_p_s0,
            self._fk_p_s1,
            self._fk_p_e0,
            self._fk_p_e1,
            self._fk_p_w0,
            self._fk_p_w1,
            self._fk_p_w2,
            self._fk_p_gripper,
            ]

        self._fk_p_kdl = PyKDL.ChainFkSolverPos_recursive(self._arm_chain)
        self._fk_v_kdl = PyKDL.ChainFkSolverVel_recursive(self._arm_chain)
        self._ik_v_kdl = PyKDL.ChainIkSolverVel_pinv(self._arm_chain)
        self._ik_p_kdl = PyKDL.ChainIkSolverPos_NR(self._arm_chain,
                                                   self._fk_p_kdl,
                                                   self._ik_v_kdl)
        self._jac_kdl = PyKDL.ChainJntToJacSolver(self._arm_chain)
        self._dyn_kdl = PyKDL.ChainDynParam(self._arm_chain,
                                            PyKDL.Vector.Zero())

    def joints_to_kdl(self, type):
        kdl_array = PyKDL.JntArray(self._num_jnts)

        if type == 'positions':
            cur_type_values = self._limb_interface.joint_angles()
        elif type == 'velocities':
            cur_type_values = self._limb_interface.joint_velocities()
        elif type == 'torques':
            cur_type_values = self._limb_interface.joint_efforts()
        for idx, name in enumerate(self._joint_names):
            kdl_array[idx] = cur_type_values[name]
        if type == 'velocities':
            kdl_array = PyKDL.JntArrayVel(kdl_array)
        return kdl_array

    def forward_position_kinematics(self, joint_angles=None):
        """
        Calculates Forward Position Kinematics
        for Baxter
        Input: joint_angles
            None for current angles
          - OR -
            dictionary of joint angles
          {'right_s0': 0.8897088559570313,
          'right_s1': -0.8548107930725098,
          'right_w0': 0.04793689956665039,
          'right_w1': 0.15033011704101565,
          'right_w2': 0.5633544437072754,
          'right_e0': -0.1714223528503418,
          'right_e1': 2.0459468735046387}
        """

        if joint_angles == None:
            # Use Current Joint Angles if none are input parameters
            kdl_array = self.joints_to_kdl('positions')
        else:
            # Use supplied dictionary for joint angles
            kdl_array = PyKDL.JntArray(self._num_jnts)
            for idx, name in enumerate(self._joint_names):
                print idx, name
                kdl_array[idx] = joint_angles[name]

        transforms = []
        print 'test'
        angles_so_far = []

        def trans_from_angles(angles, chain_piece):
            angle_array = PyKDL.JntArray(len(angles))
            for i in range(len(angles)):
                angle_array[i] = angles[i]
            current_frame = PyKDL.Frame()
            chain_piece.JntToCart(angle_array, current_frame)
            trans = Transform()
            trans.translation.x = current_frame.p[0]
            trans.translation.y = current_frame.p[1]
            trans.translation.z = current_frame.p[2]
            quat = PyKDL.Rotation(current_frame.M).GetQuaternion()
            trans.rotation.x = quat[0]
            trans.rotation.y = quat[1]
            trans.rotation.z = quat[2]
            trans.rotation.w = quat[3]
            return trans

        for angle, chain_piece in zip(kdl_array, self._chain_pieces[:-1]):
            angles_so_far.append(angle)
            transforms.append(trans_from_angles(angles_so_far, chain_piece))
        transforms.append(trans_from_angles(angles_so_far, self._chain_pieces[-1]))

        return transforms

