import rospy
import tf
import numpy as np
from geometry_msgs.msg import Pose, PoseStamped, Point, Quaternion
from gazebo_msgs.srv import SpawnModel


class PoseTransformer:
    def __init__(self, from_frame, to_frame):
        self.from_frame = from_frame
        self.to_frame = to_frame
        self.positions = []
        self.orientations = []
        self.tf = tf.TransformListener()
        self.pose_published = False
        rospy.Subscriber('/aruco_single/pose', PoseStamped, self.publish_transformed_pose)

        self._pub_object_pose = rospy.Publisher(
            '/virtual_env/object/pose/', 
            Pose, tcp_nodelay=True, queue_size=1)

    def publish_transformed_pose(self, ps_msg):
        if not self.pose_published:
            # get 10 poses to average over
            if len(self.positions) < 1:
                self.tf.waitForTransform(self.from_frame, self.to_frame, rospy.Time(), rospy.Duration(4.0))
                if self.tf.frameExists(self.from_frame) and self.tf.frameExists(self.to_frame):
                    t = self.tf.getLatestCommonTime(self.from_frame, self.to_frame)
                    position, orientation = self.tf.lookupTransform(self.from_frame, self.to_frame, t)
                    self.positions.append(position)
                    self.orientations.append(orientation)
            else:
                print 'Pose found, publishing...'
                # get average pose over 10 recorded
                self.positions = np.array(self.positions)
                self.orientations = np.array(self.orientations)
                p = np.mean(self.positions, axis=0)
                o = np.mean(self.orientations, axis=0)
                self.position = Point(x=p[0], y=p[1], z=p[2])
                self.quaternion = Quaternion(x=o[0], y=o[1], z=o[2], w=o[3])
                pose = Pose(position=self.position, orientation=self.quaternion)
                print pose
                self.pose_published = True
                # publish pose so it can be picked up by simulator
                # self._pub_object_pose.publish(pose)


def main():
    print '\nInitializing pose generator...'
    rospy.init_node('pose_generator')
    from_frame = '/brick'
    to_frame = 'left_hand_camera'
    ml = PoseTransformer(from_frame, to_frame)
    print 'Initialization complete. \nListening for marker information...'
    while not rospy.is_shutdown() and not ml.pose_published:
        rospy.sleep(0.1)


if __name__ == '__main__':
    main()
