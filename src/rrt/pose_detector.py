from geometry_msgs.msg import Point, Quaternion, PoseStamped
import ConfigParser
import os
import numpy as np
import rospy
import threading
from geometry_msgs.msg import Point, Quaternion, Pose, Vector3, Transform

class PoseDetector(object):
    def __init__(self, name, avg_count=20):
        self.mu = threading.Lock()
        self.name = name
        self.avg_count = avg_count
        self.poses = []
        self.position = None
        self.quaternion = None
        self.has_value = False
        self._load_from_file(os.path.expanduser('~/.ros/calibration.cfg'))

    def subscribe(self, topic):
        rospy.Subscriber(topic, PoseStamped, self._register_pose)

    def _load_from_file(self, config_filepath):
        config = ConfigParser.SafeConfigParser()
        config.read(config_filepath)
        section_title = 'SceneCalibration'

        if not config.has_section(section_title):
            print config.sections()
            return
        if config.has_option(section_title, self.name + '.position.x'):
            self.position = Vector3(
                x=config.getfloat(section_title, self.name + '.position.x'),
                y=config.getfloat(section_title, self.name + '.position.y'),
                z=config.getfloat(section_title, self.name + '.position.z'),
                )
            self.quaternion = Quaternion(
                x=config.getfloat(section_title, self.name + '.quaternion.x'),
                y=config.getfloat(section_title, self.name + '.quaternion.y'),
                z=config.getfloat(section_title, self.name + '.quaternion.z'),
                w=config.getfloat(section_title, self.name + '.quaternion.w'),
                )
            self.has_value = True
            self.pose = Transform(self.position, self.quaternion)

    def _register_pose(self, ps_msg):
        if len(self.poses) < self.avg_count:
            print 'Received pose for', self.name
            p = ps_msg.pose.position
            o = ps_msg.pose.orientation
            self.poses.append((
                (p.x, p.y, p.z),
                (o.x, o.y, o.z, o.w),
                ))
            if len(self.poses) >= self.avg_count:
                print 'Object {0} pose found'.format(self.name)
                positions = np.asarray([x[0] for x in self.poses])
                orientations = np.asarray([x[1] for x in self.poses])
                p = np.mean(positions, axis=0)
                o = np.mean(orientations, axis=0)
                self.position = Point(p[0], p[1], p[2])
                self.quaternion = Quaternion(o[0], o[1], o[2], o[3])
                self.poses[:] = []
                self.pose = Pose(position=self.position, orientation=self.quaternion)
