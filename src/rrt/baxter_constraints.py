import numpy as np
import tf
from copy import copy
from baxter_pykdl import baxter_kinematics

class BaxterDOFConstraints(object):
    '''
    Represents constraints over one or more Baxter degrees of freedom (DOF)

    You can use the jacobian_pseudo_inverse() function and the get_ts_error()
    function for the RRT class to be the jacobian_inv_func and
    constraint_err_func parameters respectively.
    '''

    def __init__(self, limb, constrained_dofs):
        self.bk = baxter_kinematics(limb)
        self.constraints = constrained_dofs
        self.joint_names = self.bk._joint_names

    def _convert_array_to_dict(self, q_array):
        q = copy(q_array).flatten().tolist()
        q_dict = dict(zip(self.joint_names, q))
        return q_dict

    def jacobian_pseudo_inverse(self, q):
        return self.bk.jacobian_pseudo_inverse(self._convert_array_to_dict(q))

    def get_ts_error(self, q_sample, q_near):
        """
        Compute task space error between two configuration space points.
        """
        q_sample_dict = self._convert_array_to_dict(q_sample)
        q_near_dict = self._convert_array_to_dict(q_near)

        pose_sample = self.bk.forward_position_kinematics(q_sample_dict) # x y z  x y z w
        euler = tf.transformations.euler_from_quaternion((
            pose_sample[3], pose_sample[4], pose_sample[5], pose_sample[6]
        ))
        pose_sample = np.array([
            pose_sample[0], pose_sample[1], pose_sample[2],
            euler[0], euler[1], euler[2]
        ])
        pose_near = self.bk.forward_position_kinematics(q_near_dict)
        euler = tf.transformations.euler_from_quaternion((
            pose_near[3], pose_near[4], pose_near[5], pose_near[6]
        ))
        pose_near = np.array([
            pose_near[0], pose_near[1], pose_near[2],
            euler[0], euler[1], euler[2]
        ])
        delta = pose_sample - pose_near
        ts_error = np.multiply(
            self.constraints.reshape((1, self.constraints.shape[0])),
            delta.reshape((1, delta.shape[0]))
        )
        return np.array(ts_error)

