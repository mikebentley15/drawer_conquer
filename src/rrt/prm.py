#!/usr/bin/env python
'''
Package providing helper classes and functions for performing Probabilistic
Roadmaps (PRM) for planning
'''
from collisions import PolygonEnvironment
import graph_search
import rrt

import copy
import numpy as np
import time

class Vertex(object):
    '''
    Represents a vertex in a PRM graph

    Attributes:
        loc:   location of this vertex (numpy array)
        edges: list of Edge objects with this vertex as the from
    '''
    def __init__(self, location):
        'Initialize'
        self.loc = location
        self.edges = []

class Edge(object):
    '''
    Represents a directed edge in a PRM graph

    Attributes:
        v_from: source vertex
        v_to:   destination vertex
        path:   list of locations (each a numpy array) to get from to to, each
            visited linearly.  This path does not include the from and to
            locations, so an empty path implies a straight line from to to.
    '''
    def __init__(self, from_vertex, to_vertex, path=[]):
        'Initialize edge from one vertex to another'
        self.v_from = from_vertex
        self.v_to = to_vertex
        self.path = path
        self._path_len = None

        # Add myself to the vertex object
        from_vertex.edges.append(self)
        to_vertex.edges.append(self)

    def inverted(self):
        'Returns a copy of this edge, but reversed of from and to'
        edge = Edge(self.v_to, self.v_from, list(reversed(self.path)))
        edge._path_len = self._path_len
        return edge

    def __len__(self):
        'Return the L2 distance along the path from to to'
        if self._path_len is None:
            current = self.v_from.loc
            self._path_len = 0
            for loc in (self.path + [self.v_to.loc]):
                self._path_len += np.linalg.norm(loc - current)
                current = loc
        return self._path_len

class PRMGraph(object):
    '''
    Represents a Probabilistic Road Map (PRM) graph that allows us to travel in
    the space collision-free.

    Attributes:
        vertices:  list of vertices.  The vertices have their edges already.
        locations: set of vertex locations for quick lookup checks
    '''
    def __init__(self):
        'Initialize an empty PRM graph'
        self.vertices = []
        self.locations = set()
        self.edges = []

    def add_edge(self, edge):
        'Adds the edge to the graph and its vertices if not already there'
        if tuple(edge.v_from.loc) not in self.locations:
            self.locations.add(tuple(edge.v_from.loc))
            self.vertices.append(edge.v_from)
        if tuple(edge.v_to.loc) not in self.locations:
            self.locations.add(tuple(edge.v_to.loc))
            self.vertices.append(edge.v_to)
        self.edges.append(edge)

    def add_undirected_edge(self, edge):
        'Adds the edge in both directions'
        self.add_edge(edge)
        self.add_edge(edge.inverted())

    def remove_vertex(self, vertex):
        'Removes the selected vertex from the graph'
        self.vertices.remove(vertex)
        for i, edge in enumerate(self.edges):
            if edge.v_from is vertex or edge.v_to is vertex:
                self.edges.pop(i)
                edge.v_from.edges.remove(edge)
                edge.v_to.edges.remove(edge)

class PRM(object):
    'Solver for a PRM'
    def __init__(self, num_samples, num_dimensions, k, step_size, lims,
                 collision_func=None, local_planner=None, progress_callback=None):
        '''
        Initialize a PRM planning instance
        
        num_samples:    max samples to generate
        num_dimensions: size of configuration space
        k:              neighbors to connect
        lims:           limits of the workspace for each dimension
        step_size:      size of steps for the local_planner
        collision_func: to check for collision.
            params:  q - a configuration
            return:  bool
            default: Nothing is in collision
        local_planner:  to connect two points together
            params:  p, q - two configurations
            return:  piecewise linear path from p to q or None if not found
            default: local linear binary search planner
        '''
        self.graph = PRMGraph()
        self.n = num_samples
        self.dim = num_dimensions
        self.k = k
        self.step_size = step_size
        self.limits = lims
        self.lower_limits = lims[:, 0]
        self.upper_limits = lims[:, 1]
        self.is_collision = collision_func
        if collision_func is None:
            self.is_collision = self.fake_in_collision
        self.local_planner = local_planner
        if local_planner is None:
            self.local_planner = self.linear_plan
        self.progress_callback = progress_callback
        self._is_built = False

    def build_prm(self):
        '''
        Build the PRM with uniform distribution.  This simply attempts to map
        out the space.
        '''
        return self._build_prm_impl(self.uniform_sample)

    def build_prm_gaussian(self, stddev):
        '''
        Build the PRM with gaussian sampling technique.  This simply attempts
        to map out the space.
        '''
        # TODO: make the standard deviation decrease with steps
        #diff = stddev / self.n
        #def sampler():
        #    'Updates the standard deviation and then gaussian samples'
        #    stddev -= diff
        #    return self.gaussian_sample(stddev)
        return self._build_prm_impl(lambda: self.gaussian_sample(stddev))

    def _build_prm_impl(self, sampler):
        'Implementation to generate the PRM'
        assert not self._is_built, 'You should not build the PRM more than once'
        for i in xrange(self.n):
            sample = sampler()
            self.extend(sample)
            self.progress_callback(100 * float(i) / float(self.n))
        self.progress_callback(100)
        self._is_built = True

    def get_full_graph(self):
        '''
        Returns the full graph including the path points between vertices.
        This returns both the graph locations and the edges.  The locations are
        simply arrays in configuration space.  The edges are two-tuples of
        locations (from, to).

        >>> Qs, edges = planner.get_full_graph()
        '''
        assert self._is_built, 'The graph is not built'
        coarse_edges = self.graph.edges
        visited_edges = set()
        visited_vertices = set()
        fine_edges = []
        Qs = []
        for edge in coarse_edges:
            # Make sure we haven't already counted the other directional edge
            if (edge.v_from, edge.v_to) in visited_edges:
                continue
            visited_edges.add((edge.v_from, edge.v_to))
            visited_edges.add((edge.v_to, edge.v_from))
            if edge.v_to not in visited_vertices:
                visited_vertices.add(edge.v_to)
                Qs.append(edge.v_to.loc)
            current = edge.v_from.loc
            for loc in (edge.path + [edge.v_to.loc]):
                fine_edges.append((current, loc))
                current = loc
        for vertex in self.graph.vertices:
            if vertex not in visited_vertices:
                visited_vertices.add(vertex)
                Qs.append(vertex.loc)
        return (Qs, fine_edges)

    def find_path(self, init, goal):
        '''
        To be called after one of the build_prm* functions.  This tries to find
        a path using the generated PRM from init to goal.

        Returns a list of locations from start to goal.
        '''
        assert self._is_built, 'The graph is not built'

        # connect init and goal to the grid
        init_state = self.extend(init)
        goal_state = self.extend(goal)

        f = lambda vertex, edge: edge.v_to
        is_goal = lambda vertex: np.all(vertex.loc == goal)
        actions_func = lambda vertex: vertex.edges
        h = lambda vertex: np.linalg.norm(goal - vertex.loc)
        cost_func = lambda v_from, edge, v_to: len(edge)

        plan, _ = graph_search.a_star_search(init_state, f, is_goal,
                actions_func, h, cost_func)

        # remove init and goal from the PRM now that it's done
        self.graph.remove_vertex(init_state)
        self.graph.remove_vertex(goal_state)

        if plan is not None:
            fine_plan = []
            for edge in plan[1]:
                fine_plan.append(edge.v_from.loc)
                fine_plan.extend(edge.path)
            fine_plan.append(goal)

            return fine_plan
        else:
            return None

    def uniform_sample(self):
        'Generates and returns a random sample uniformly in limits'
        return np.random.uniform(self.lower_limits, self.upper_limits)

    def gaussian_distribution(self, mean, stddev):
        '''
        Generates and returns a random sample from a gaussian distribution
        truncated to limits
            mean:   numpy array of mean values
            stddev: numpy array of standard deviation for each dimension
        '''
        val = np.random.normal(mean, stddev)
        val = np.minimum(val, self.upper_limits)
        val = np.maximum(val, self.lower_limits)
        return val

    def gaussian_sample(self, stddev):
        '''
        Generates and returns a sample using the Gaussian method.  This first
        generates a uniform sample, then a sample from a Gaussian centered
        about the first sample with stddev.  Return one of the two samples if
        one and only one of the samples is not in collision (and return that
        one).  Otherwise return None.
        '''
        attempts = 0
        max_attempts = 20
        sample = None
        while sample is None and attempts < max_attempts:
            attempts += 1
            center = self.uniform_sample()
            other = self.gaussian_distribution(center, stddev)
            center_is_collision = self.is_collision(center)
            other_is_collision = self.is_collision(other)

            if center_is_collision != other_is_collision:
                sample = other if center_is_collision else center
        return sample

    def extend(self, q):
        '''
        Perform PRM extend operation.
        q - new point to add to the PRM
        Returns the vertex containing q
        '''
        #print 'extend({0})'.format(q)
        if q is None:
            return
        new_vertex = Vertex(q)
        for neighbor in self.nearest_neighbors(q):
            path = self.local_planner(q, neighbor.loc)
            if path is not None:
                self.graph.add_undirected_edge(Edge(new_vertex, neighbor, path))
        if not self.is_collision(q):
            self.graph.vertices.append(new_vertex)
        return new_vertex

    def nearest_neighbors(self, q):
        'Find and return the k nearest neighbors to the location q as vertices'
        # TODO: implement more efficiently using k-d tree
        distances = []
        for vertex in self.graph.vertices:
            distances.append((vertex, np.linalg.norm(q - vertex.loc)))
        distances.sort(key=lambda x: x[1])
        return [x[0] for x in distances[:self.k]]

    def fake_in_collision(self, q):
        'We never collide with this function!'
        return False

    def linear_plan(self, source, dest):
        '''
        Tries to connect source and dest linearly
        
        If successful, then returns the edge object with the plan.  Otherwise
        returns None to signify that we could not find a plan.
        '''
        # TODO: implement more efficiently using binary linear plan
        diff = dest - source
        unit_diff = diff / np.linalg.norm(diff)
        distance = np.linalg.norm(diff)
        n = int(np.ceil(distance / self.step_size))

        locations = [source + i * unit_diff for i in xrange(n)]
        locations.append(dest)

        for loc in locations:
            if self.is_collision(loc):
                return None
        return []

def test_prm_env(num_samples=500, step_length=2, k=4, env='./env0.txt',
                 use_rrt=False, rrt_connect_prob=0.05, rrt_n=None,
                 use_gaussian_sampling=False, gaussian_stddev=10,
                 skip_solution=False, progress_callback=None):
    '''
    create an instance of PolygonEnvironment from a description file and plan a
    path from start to goal on it using an RRT

    num_samples - number of samples to generate in RRT
    step_length - step size for growing in rrt (epsilon)
    env - path to the environment file to read
    connect - If True run rrt_connect

    returns plan, planner - plan is the set of configurations from start to
      goal, planner is the rrt used for building the plan
    '''
    pe = PolygonEnvironment()
    pe.read_env(env)

    dims = len(pe.start)
    start_time = time.time()

    local_planner = None
    if use_rrt:
        if rrt_n is None:
            rrt_n = num_samples
        def rrt_local_planner(source, dest):
            planner = rrt.RRT(rrt_n,
                              dims,
                              step_length,
                              lims=pe.lims,
                              connect_prob=rrt_connect_prob,
                              collision_func=pe.test_collisions)
            return planner.build_rrt(source, dest)
        local_planner = rrt_local_planner

    prm = PRM(num_samples,
              dims,
              k,
              step_length,
              pe.lims,
              collision_func=pe.test_collisions,
              local_planner=local_planner,
              progress_callback=progress_callback
              )

    if use_gaussian_sampling:
        prm.build_prm_gaussian(gaussian_stddev)
    else:
        prm.build_prm()
    print 'len(prm.graph.vertices) =', len(prm.graph.vertices)
    print 'len(prm.graph.edges)    =', len(prm.graph.edges)
    plan = None
    if not skip_solution:
        plan = prm.find_path(pe.start, pe.goal)

    run_time = time.time() - start_time
    print 'plan:', plan
    print 'run_time =', run_time
    return plan, prm, pe
