#!/usr/bin/env python
'''
Package providing helper classes and functions for performing graph search
operations for planning.
'''
import numpy as np
import random
import time
import tf
import sys
from collisions import PolygonEnvironment, RectRobot
from copy import copy

_DEBUG = False

_TRAPPED = 'trapped'
_ADVANCED = 'advanced'
_REACHED = 'reached'

class TreeNode(object):
    'Represents a tree node'
    def __init__(self, state, parent=None):
        'initializer'
        self.state = state
        self.children = []
        self.parent = parent

    def add_child(self, child):
        'Adds child'
        self.children.append(child)

class RRTSearchTree(object):
    'Search tree used by RRT'
    def __init__(self, init):
        'initializer'
        self.root = TreeNode(init)
        self.nodes = [self.root]
        self.edges = []

    def find_nearest(self, s_query):
        'Returns the nearest node to s_query'
        min_d = 1000000
        nn = self.root
        for n_i in self.nodes:
            d = np.linalg.norm(s_query - n_i.state)
            if d < min_d:
                nn = n_i
                min_d = d
        return (nn, min_d)

    def add_node(self, node, parent):
        'Adds a node to the tree'
        self.nodes.append(node)
        self.edges.append((parent.state, node.state))
        node.parent = parent
        parent.add_child(node)

    def get_states_and_edges(self):
        'Returns the list of states and edges'
        states = np.array([n.state for n in self.nodes])
        return (states, self.edges)

    def get_back_path(self, n):
        'Return the path from the root to n'
        path = []
        while n.parent is not None:
            path.append(n.state)
            n = n.parent
        # add the root state
        path.append(n.state)
        path.reverse()
        return path

class RRT(object):
    'Rapidly exploring Random Tree'
    _IN_COLLISION = 1
    _REACHED = 2
    _EXTENDED = 3

    def __init__(
            self,
            num_samples,
            step_length=1,
            lims=None,
            connect_prob=0.05,
            collision_func=None,
            constraint_err_func=None,
            jacobian_inv_func=None,
            ):
        '''
        Initialize an RRT planning instance

        step_length
            (float) Size of step in each direction
        lims
            (list of 2-tuples) limits for each configuration space dimension
        connect_prob
            (float between 0.0 and 1.0) probability of extending to the goal
        collision_func
            (func (config_array) -> bool) returns True if given configuration
            is in collision
        constraint_err_func
            (func (config_array sample, config_array near) -> config_array error)
            If you want to have additional workspace constraints, then pass in
            this function.  If None, then no additional constraints will be
            used.
        jacobian_inv_func
            (func (config_array sample) -> np.matrix jacobian_inverse)
            Returns the jacobian pseudo-inverse.  Use this if you want to move
            samples to the constraint manifold rather than simply rejecting
            samples that are too far away from the manifold.  This value is
            ignored if the constraint_err_func is None.
        '''
        self.K = num_samples
        self.epsilon = step_length
        self.connect_prob = connect_prob
        self.goal = None
        self.T = None

        self.in_collision = collision_func
        if collision_func is None:
            self.in_collision = self.fake_in_collision

        # Setup range limits
        self.limits = lims

        self.ranges = self.limits[:, 1] - self.limits[:, 0]
        self.found_path = False
        self.constraint_err_func = constraint_err_func
        self.jacobian_inv_func = jacobian_inv_func

        if self.constraint_err_func is None:
            self.extend_func = self.extend
        elif self.jacobian_inv_func is None:
            self.extend_func = self.constrained_extend_reject
        else:
            self.extend_func = self.constrained_extend

    def smooth_path(self, path, num_trials=None):
        'Smooths the path after the RRT.  Number of trials defaults to path length'
        # Try to take shortcuts
        before = time.time()
        path = copy(path)
        trial_count = len(path)
        for _ in range(trial_count):
            samples = random.sample(path, 2)
            idx_1 = [i for i in range(len(path)) if np.all(samples[0] == path[i])][0]
            idx_2 = [i for i in range(len(path)) if np.all(samples[1] == path[i])][0]
            if idx_1 > idx_2:
                samples.reverse()
                idx_1, idx_2 = idx_2, idx_1
            inner_path = self.build_straight_line_path(samples[0], samples[1])
            if inner_path is not None:
                path = path[0:idx_1] + inner_path + path[idx_2+1:]
        # TODO: spline paths
        print 'Smoothing time:', time.time() - before
        return path

    def build_straight_line_path(self, init, goal):
        '''
        Build a straight line path.  Only return it if there are no collisions,
        otherwise return None.
        '''
        self.goal = np.array(goal)
        self.init = np.array(init)
        self.T = RRTSearchTree(init)
        extend_count = 0
        while extend_count < self.K:
            # since self.extend() deals with constraints, this function
            # handles constraints automatically
            status = self.extend_func(self.goal)
            extend_count += 1
            if status == self._REACHED:
                return self.T.get_back_path(self.T.nodes[-1])
            elif status == self._IN_COLLISION:
                return None

    def build_straight_line_path_2(self, init, goal):
        '''
        Build a straight line path.  Only return it if there are no collisions,
        otherwise return None.
        '''
        self.goal = np.array(goal)
        self.init = np.array(init)
        extend_count = 0
        while extend_count < self.K:
            status = self.extend_func(self.goal)
            extend_count += 1
            if status == self._REACHED:
                return False
            elif status == self._IN_COLLISION:
                return True

    def build_rrt(self, init, goal):
        '''
        Build the rrt from init to goal
        Returns path to goal or None
        '''
        self.goal = np.array(goal)
        self.init = np.array(init)
        self.found_path = False

        if self.in_collision(self.goal):
            return None

        # Build tree and search
        self.T = RRTSearchTree(init)
        path = None
        extend_count = 0
        while extend_count < self.K:
            x = self.sample()
            status = self.extend_func(x)
            extend_count += 1
            if status == self._REACHED:
                if np.all(x == self.goal):
                    # Here we assume that x is the last node added
                    path = self.T.get_back_path(self.T.nodes[-1])
                    self.found_path = True
                    break

        return path

    def build_rrt_connect(self, init, goal):
        '''
        Build the rrt connect from init to goal
        Returns path to goal or None
        '''
        self.goal = np.array(goal)
        self.init = np.array(init)
        self.found_path = False

        if self.in_collision(self.goal):
            return None

        # Build tree and search
        self.T = RRTSearchTree(init)
        path = None
        while len(self.T.nodes) < self.K:
            x = self.sample()
            status = self._EXTENDED
            while status is self._EXTENDED:
                status = self.extend_func(x)
            if status == self._REACHED:
                if np.all(x == self.goal):
                    # Here we assume that x is the last node added
                    path = self.T.get_back_path(self.T.nodes[-1])
                    self.found_path = True
                    break

        return path

    def build_rrt_connect_bidirectional(self, init, goal):
        '''
        Build the rrt connect from init to goal from both directions
        Returns path to goal or None
        '''
        self.goal = np.array(goal)
        self.init = np.array(init)
        self.found_path = False

        if self.in_collision(self.goal):
            return None

        # Build tree and search
        T1 = RRTSearchTree(self.init)
        T2 = RRTSearchTree(self.goal)
        TTot = RRTSearchTree(self.init)
        TTot.nodes.append(T2.root)
        path = None
        while len(T1.nodes) + len(T2.nodes) < self.K:
            # First do a single extend on T2
            self.T = T2
            self.goal = T1.root.state
            x = self.sample()
            status = self.extend_func(x)

            # Now if the extend succeeded, then do a connect extend from T1 to
            # the new node in T2
            if status != self._IN_COLLISION:
                TTot.nodes.append(T2.nodes[-1])
                TTot.edges.append(T2.edges[-1])
                x_new = T2.nodes[-1]
                self.T = T1
                self.goal = x_new
                status = self._EXTENDED
                while status is self._EXTENDED:
                    status = self.extend_func(x_new.state)
                    if status != self._IN_COLLISION:
                        TTot.nodes.append(T1.nodes[-1])
                        TTot.edges.append(T1.edges[-1])
                if status == self._REACHED:
                    # Here we assume that x is the last node added
                    path = T1.get_back_path(T1.nodes[-1])
                    path.extend(reversed(T2.get_back_path(x_new)))
                    if not np.all(path[0] == init):
                        path.reverse()
                    self.found_path = True
                    break
            # Occasionally swap the two trees
            if len(T1.nodes) > len(T2.nodes):
                T1, T2 = T2, T1

        self.T = TTot
        self.goal = goal
        return path

    def sample(self):
        '''
        Sample a new configuration and return
        '''
        # Return goal with connect_prob probability
        if np.random.random_sample() <= self.connect_prob:
            return self.goal
        else:
            sample = self.limits[:, 0] + self.ranges * np.random.random_sample(len(self.goal))
            return sample

    def extend(self, q):
        '''
        Perform rrt extend operation.
        q - new configuration to extend towards
        '''
        status = None
        near_node, distance = self.T.find_nearest(q)
        if distance <= self.epsilon:
            new_point = q
            status = self._REACHED
        else:
            direction = q - near_node.state
            # Make it a unit vector
            direction /= np.linalg.norm(direction)
            new_point = near_node.state + direction * self.epsilon
            status = self._EXTENDED

        if self.in_collision(new_point):
            status = self._IN_COLLISION
        else:
            self.T.add_node(TreeNode(new_point), near_node)

        return status

    def constrained_extend(self, q_rand, manifold_sampler=None):
        """
        Perform extend operation along a constraint manifold by taking a random
        sample and pushing it to the manifold according to the manifold_sampler.
        """
        if manifold_sampler is None:
            manifold_sampler = self.nearest_to_manifold_FR
        status = None
        near_node, distance = self.T.find_nearest(q_rand)
        q_near = np.array(near_node.state)
        q_rand_manifold = manifold_sampler(q_rand, q_near) # so connect algorithm can work
        q_manifold = None
        if q_rand_manifold is not None:
            distance = np.linalg.norm(q_rand_manifold - q_near) # two points on manifold
            if distance < self.epsilon:
                q_manifold = q_rand_manifold
                status = self._REACHED
            else:
                q_dir = q_rand_manifold - q_near  # two points on manifold
                q_dir /= np.linalg.norm(q_dir)
                q_sample = q_near + q_dir * self.epsilon
                q_manifold = manifold_sampler(q_sample, q_near)
                status = self._EXTENDED
        if q_manifold is not None:
            if self.in_collision(q_manifold):
                status = self._IN_COLLISION
            else:
                self.T.add_node(TreeNode(q_manifold.flatten()), near_node)
        return status

    def nearest_to_manifold_FR(self, q_sample, q_near, ts_epsilon=0.01, gamma=0.01):
        """
        Push a random sample to the constraint manifold according
        to the First-Order Retraction algorithm.
        """
        #print 'nearest_to_manifold_FR inputs:', q_sample, q_near
        #print '  q_sample.shape:', q_sample.shape
        sys.stdout.write('.')
        sys.stdout.flush()
        #if q_sample.shape[0] != 1:
        #    q_sample = q_sample.reshape((1, q_sample.shape[0]))
        #    print '  reshaped sample:', q_sample
        q_orig = copy(q_sample)
        ts_err = self.constraint_err_func(q_sample, q_near)
        while np.linalg.norm(ts_err) > ts_epsilon:
            jpi = self.jacobian_inv_func(q_sample)
            cs_err = jpi.dot(ts_err.reshape((ts_err.size, 1)))
            cs_err = cs_err.reshape(cs_err.size)
            # update sample with configuration space error
            q_sample -= (gamma * cs_err)
            ts_err = self.constraint_err_func(q_sample, q_near)
        if self.in_collision(q_sample):
            return None
        else:
            return q_sample

    def constrained_extend_reject(self, q_rand):
        """
        Randomly sample points as usual but reject them if
        they are not close enough to the constraint manifold.
        """
        status = None
        near_node, distance = self.T.find_nearest(q_rand)
        q_near = near_node.state
        if distance <= self.epsilon:
            q_sample = q_rand
            status = self._REACHED
        else:
            q_dir = q_rand - q_near
            q_dir /= np.linalg.norm(q_dir)
            q_sample = q_near + q_dir * self.epsilon
            status = self._EXTENDED

        if self.in_collision(q_sample):
            status = self._IN_COLLISION
        elif self.near_manifold(q_sample, q_near):
            self.T.add_node(TreeNode(q_sample), near_node)
        else:
            # Note: self._OFF_MANIFOLD is never defined or used.  Just use
            #   _IN_COLLISION for now to mean "don't use this sample"
            #status = self._OFF_MANIFOLD
            status = self._IN_COLLISION
        return status

    def near_manifold(self, q_sample, q_near, manifold_epsilon=0.015):
        """
        Determine if a sampled point is within manifold_epsilon distance
        from the manifold by converting task space error to configuration space.
        """
        ts_error = np.linalg.norm(self.constraint_error_func(q_sample, q_near))
        return ts_error < manifold_epsilon

    def fake_in_collision(self, q):
        '''
        We never collide with this function!
        '''
        return False

def test_rrt_env(
        num_samples=500,
        step_length=2,
        env='./env0.txt',
        connect=False,
        connect_prob=0.05,
        bidirectional=False,
        constraints=None,
        ):
    '''
    create an instance of PolygonEnvironment from a description file and plan a
    path from start to goal on it using an RRT

    num_samples - number of samples to generate in RRT
    step_length - step size for growing in rrt (epsilon)
    env - path to the environment file to read
    connect - If True run rrt_connect

    returns plan, planner, env - plan is the set of configurations from start to
      goal, planner is the rrt used for building the plan, env is the world
      environment for chopping and stuff.
    '''
    pe = PolygonEnvironment()
    pe.read_env(env)

    dims = len(pe.start)
    start_time = time.time()

    err_func = None
    inv_jac = None
    if constraints is not None:
        fk_pos = lambda q: pe.robot.fk(q)[-1]
        if isinstance(pe.robot, RectRobot):
            fk_angle = lambda q: 0
            inv_jac = lambda q: np.asarray([[1, 0, 0], [0, 1, 0]])
        else:
            fk_angle = lambda q: q.sum()
            inv_jac = pe.robot.jacobian_inverse
        fk = lambda q: np.asarray(fk_pos(q).tolist() + [fk_angle(q)])
        err_func = lambda q_s, q_n: constraints * (fk(q_s) - fk(q_n))
        

    rrt = RRT(num_samples,
              step_length=step_length,
              lims=pe.lims,
              connect_prob=connect_prob,
              collision_func=pe.test_collisions,
              constraint_err_func=err_func,
              jacobian_inv_func=inv_jac,
              )

    if connect:
        if bidirectional:
            plan = rrt.build_rrt_connect_bidirectional(pe.start, pe.goal)
        else:
            plan = rrt.build_rrt_connect(pe.start, pe.goal)
    else:
        plan = rrt.build_rrt(pe.start, pe.goal)
    run_time = time.time() - start_time
    print 'plan:', plan
    print 'run_time =', run_time
    return plan, rrt, env
