#!/usr/bin/env python
'''
Package providing helper classes and functions for performing graph search
operations for planning.

This was copied from hw01 and modified for the new problem.
'''
import numpy as np
import heapq
import collections

VERBOSE_1 = True
VERBOSE_2 = False
VERBOSE_3 = False

class SearchNode(object):
    def __init__(self, s, A, parent=None, parent_action=None, cost=0, depth=0):
        '''
        s - the state defining the search node
        A - list of actions
        parent - the parent search node
        parent_action - the action taken from parent to get to s
        '''
        self.parent = parent
        self.cost = cost
        self.parent_action = parent_action
        self.depth = depth
        self.state = s
        self.actions = A[:]

    def __str__(self):
        '''
        Return a human readable description of the node
        '''
        return ' '.join([
            'state:', str(self.state),
            ' parent:', str(None if self.parent is None else self.parent.state),
            ' action:', str(self.parent_action),
            ' cost:', str(self.cost),
            ' depth:', str(self.depth),
            ])

class Storage(object):
    'Base class for storage.  Provides len() and str()'
    def _storage(self):         return None
    def __len__(self):          return len(self._storage())
    def __str__(self):          return str(self._storage())
    def __contains__(self, x):  return x in self._storage()

class SimpleSet(Storage):
    'Simply a set of the node names'
    def __init__(self):         self.states = set()
    def add(self, node):        self.states.add(node.state)
    def remove(self, x):        self.states.remove(x.state)
    def _storage(self):         return self.states
    def __contains__(self, x):  return x.state in self.states

class PrioritySet(SimpleSet):
    '''
    A set that has elements, but also a priority criteria.  The idea is that an
    element is considered in the set if it has no worse priority than the one
    already in the set.  A lower number is considered high priority.
    '''
    def __init__(self, cost_func=None):
        '''
        Initializes the PrioritySet as empty.
        
        The cost_func is a function taking a SearchNode and returning a score.
        A lower score is considered higher priority.
        '''
        self._d = {}
        self.cost_func = cost_func
        if cost_func is None:
            self.cost_func = lambda x: x.cost

    @property
    def states(self):         return set(self._d.keys())
    def remove(self, x):       del(self._d[x.state])

    def __contains__(self, x):
        return x.state in self._d and self.cost_func(x) >= self._d[x.state]

    def add(self, node):
        if node not in self:
            self._d[node.state] = self.cost_func(node)

class Stack(Storage):
    'LIFO structure implementing only push(), pop(), and peak()'
    def __init__(self):         self._s = []
    def _storage(self):         return self._s
    def push(self, node):       self._s.append(node)
    def pop(self):              return self._s.pop()
    def peak(self):             return None if len(self._s) == 0 else self._s[-1]

class DepthLimitedStack(Stack):
    '''
    LIFO structure with limited depth.  Each element is expected to have a
    depth attribute
    '''
    def __init__(self, depth_limit):
        super(DepthLimitedStack, self).__init__()
        self.depth_limit = depth_limit
        self.reject_count = 0

    def push(self, node):
        'Only pushes if the depth is less than or equal to the limit'
        if node.depth <= self.depth_limit:
            self._s.append(node)
        else:
            self.reject_count += 1

class Q(Storage):
    'FIFO structure implementing only push(), pop(), and peak()'
    def __init__(self):         self._q = collections.deque()
    def _storage(self):         return self._q
    def push(self, node):       self._q.appendleft(node)
    def pop(self):              return self._q.pop()
    def peak(self):             return None if len(self._q) == 0 else self._q[-1]

class PriorityQ(Storage):
    '''
    Priority queue implementing only push(), pop(), and peak().

    Elements added are expected to have a state attribute such as the
    SearchNode class.  Each node is supposed to have a cost, either from a
    passed in cost function or the default cost attribute.  The cost is what
    determines priority.  The lower the cost, the higher the priority.
    '''
    def __init__(self, cost_func=None):
        '''
        Initializes the priority queue.  If cost_func is None, then the cost
        attribute will be used on elements
        '''
        self._l = []    # list storing the priority q
        self._d = {}    # dictionary of state -> cost
        self.cost_func = cost_func
        if cost_func is None:
            self.cost_func = lambda x: x.cost

    def _storage(self):      return self._l
    def peak(self):          return self._l[0][1]

    def push(self, node):
        '''
        Adds an element to the priority queue.
        If the state already exists, we replace it with this new one
        '''
        cost = self.cost_func(node)
        if node.state in self._d:
            if cost < self._d[node.state]:
                if VERBOSE_3:  print '  replace:',
                return self._replace(node)
            else:
                return
        if VERBOSE_3: print 'PriorityQ: push', node.state, cost
        heapq.heappush(self._l, (cost, node))
        self._d[node.state] = cost

    def pop(self):
        'Get the value and remove the lowest cost element from the queue'
        _, x = heapq.heappop(self._l)
        if VERBOSE_3: print 'PriotiryQ: pop', x.state, self.cost_func(x)
        del(self._d[x.state])
        return x

    def _replace(self, x):
        'Removes element x from the q and replaces it with x with the new_cost'
        for y in self._l:
            if x.state == y[1].state:
                self._l.remove(y)
                del(self._d[y[1].state])
                break
        heapq.heapify(self._l)
        self.push(x)

def _search_implementation(init_state, f, is_goal, actions_func, frontier,
                           visited=None, cost_func=None):
    '''
    Performs a search on a grid map.  The search will behave differently
    depending on the passed in frontier data structure.

    If no solution is found, this will return (None, visited)

    init_state - the intial state on the map
    f - transition function of the form s_prime = f(s,a)
    is_goal - function taking as input a state s and returning True if its a
        goal state
    actions_func - function taking a state and returning set of actions which
        can be taken by the agent
    frontier - structure for storing our frontier nodes.  This will be storing
        and working with SearchNode objects.  This needs to be empty when
        given.  It needs the following functions:
        - push(node): add the node to the frontier
        - pop(): get the next node to expand.  This is where the behavior may
            differ
    visited - A structure with add() and __contains__().  This is to keep track
        of the visited nodes.  It will be passed the SearchNode object.  It
        also needs an attribute called states that is the set of states (not
        nodes) visited.
    cost_func - function taking state, action, state (i.e. (s, a, s')) and
        returning the score of the action cost from s to s'.  If None is
        passed, then all actions will have cost 1

    returns - ((path, action_path), visited) or None if no path can be found
    path - a list of tuples. The first element is the initial state followed by
        all states traversed until the final goal state
    action_path - the actions taken to transition from the initial state to
        goal state
    visited - A set of visited states during the search
    '''
    if VERBOSE_2: print 'Starting search', init_state
    if visited is None: visited = SimpleSet()
    if cost_func is None: cost_func = lambda s, a, sp: 1
    start = SearchNode(init_state, actions_func(init_state))
    frontier.push(start)
    while len(frontier) > 0:
        current = frontier.pop()
        if VERBOSE_2:
            print '  current path:  ', ' -> '.join(str(x) for x in backpath(current)[0])
        if current in visited: continue
        visited.add(current)
        if is_goal(current.state):
            return (backpath(current), visited.states)
        for action in current.actions:
            next_state = f(current.state, action)
            next_node = SearchNode(next_state, actions_func(next_state),
                    parent=current,
                    parent_action=action,
                    cost=current.cost + cost_func(current.state, action, next_state),
                    depth=current.depth + 1,
                    )
            if next_node not in visited:
                if VERBOSE_2: print 'next_state:', next_state
                frontier.push(next_node)
    return (None, visited.states)

def dfs(init_state, f, is_goal, actions_func, cost_func=None):
    '''
    Perform depth first search on a grid map.

    init_state - the intial state on the map
    f - transition function of the form s_prime = f(s,a)
    is_goal - function taking as input a state s and returning True if its a goal state
    actions_func - function taking a state and returning set of actions which
        can be taken by the agent

    returns - ((path, action_path), visited) or None if no path can be found
    path - a list of tuples. The first element is the initial state followed by all states
        traversed until the final goal state
    action_path - the actions taken to transition from the initial state to goal state
    '''
    frontier = Stack()
    return _search_implementation(init_state, f, is_goal, actions_func, frontier, cost_func=cost_func)

def iterative_deepening(init_state, f, is_goal, actions_func, cost_func=None):
    paths = None
    visited = set()
    depth = 0
    rejected_count = -1
    # We know to stop if we have an answer
    # If there is no solution, we can monitor the number of nodes rejected from
    # the frontier.  When the amount rejected is zero, then we have visited all
    # reachable nodes.
    while paths is None and rejected_count != 0:
        depth += 1
        frontier = DepthLimitedStack(depth)
        paths, local_visited = _search_implementation(
                init_state,
                f,
                is_goal,
                actions_func,
                frontier,
                visited=PrioritySet(lambda x: x.depth),
                cost_func=cost_func,
                )
        visited.update(local_visited)
        rejected_count = frontier.reject_count
    return (paths, visited)

def bfs(init_state, f, is_goal, actions_func, cost_func=None):
    '''
    Perform breadth first search on a grid map.

    init_state - the intial state on the map
    f - transition function of the form s_prime = f(s,a)
    is_goal - function taking as input a state s and returning True if its a goal state
    actions_func - function taking a state and returning set of actions which
        can be taken by the agent

    returns - ((path, action_path), visited) or None if no path can be found
    path - a list of tuples. The first element is the initial state followed by all states
        traversed until the final goal state
    action_path - the actions taken to transition from the initial state to goal state
    '''
    frontier = Q()
    return _search_implementation(init_state, f, is_goal, actions_func, frontier, cost_func=cost_func)

def uniform_cost_search(init_state, f, is_goal, actions_func, cost_func=None):
    frontier = PriorityQ()
    visited = PrioritySet()
    return _search_implementation(init_state, f, is_goal, actions_func, frontier, visited, cost_func=cost_func)

def a_star_search(init_state, f, is_goal, actions_func, h, cost_func=None):
    '''
    init_state - value of the initial state
    f - transition function takes input state (s), action (a), returns s_prime = f(s, a)
        returns s if action is not valid
    is_goal - takes state as input returns true if it is a goal state
    actions_func - function taking a state and returning set of actions which
        can be taken by the agent
    h - heuristic function, takes input s and returns estimated cost to goal
    '''
    frontier_cost_func = lambda x: x.cost + h(x.state)
    frontier = PriorityQ(frontier_cost_func)
    return _search_implementation(init_state, f, is_goal, actions_func, frontier, cost_func=cost_func)

def backpath(node):
    '''
    Function to determine the path that lead to the specified search node

    node - the SearchNode that is the end of the path

    returns - a tuple containing (path, action_path) which are lists respectively of the states
    visited from init to goal (inclusive) and the actions taken to make those transitions.
    '''
    path = []
    action_path = []
    while node.parent is not None:
        path.append(node.state)
        action_path.append(node.parent_action)
        node = node.parent
    path.append(node.state)  # Add the initial state
    path.reverse()
    action_path.reverse()
    return (path, action_path)
