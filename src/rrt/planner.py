#!/usr/bin/env python

import collisions
import prm
import rrt

import argparse
import numpy as np
import os
import sys

g_algorithms = [
    'RRT',
    'RRT-connect',
    'RRT-biconnect',
    'PRM',
    'PRM-gauss',
    'PRM-localRRT',
    'PRM-gauss-localRRT',
    ]

def parse_args(arguments):
    parser = argparse.ArgumentParser(
            description='Generates plans in configuration space for a given '
                'environment')
    parser.add_argument('env', help='environment file to analyze')
    parser.add_argument('-n', '--num-samples', type=int, default=5000,
            help='Maximum number of nodes')
    parser.add_argument('-s', '--step-length', type=float, default=2.0,
            help='How big of steps to take in configuration space.  Default is 2.0.')
    parser.add_argument('-a', '--algorithm',
            default='RRT',
            help='Which algorithm to use for building a search graph. '
                 'Choices are ' + ', '.join(g_algorithms) + '. '
                 'Default value is RRT.')
    parser.add_argument('-d', '--display', action='store_true',
            help='Display the resultant graph and discovered path')
    parser.add_argument('-b', '--bias', type=float, default=0.05,
            help='How much to bias search toward the goal.  A number between 0 '
                 'and 1.  Default is 0.05.')
    parser.add_argument('-o', '--output-image', default=None,
            help='Filename for outputting an image of the environment, tree and path.')
    parser.add_argument('-g', '--plot-graph', action='store_true',
            help='Plot the graph or tree without the plan superimposed first.'
                 'You will need to close the first window for the next one to'
                 'be generated.')
    parser.add_argument('-S', '--smooth', action='store_true',
            help='This will attempt to smooth the path after running the algorithm.'
                 'Currently only works with the RRT algorithms.')
    parser.add_argument('-C', '--constraints', type=int, nargs=3, metavar='N',
            help='Task space constraints of the end effector.  Should be 6'
                 'integers of either zero or one in order of x y theta'
                 'where the latter is the angle incident to the x-axis.')
    args = parser.parse_args(arguments)
    assert args.algorithm in g_algorithms, 'Invalid algorithm chosen'
    return args

def main(arguments):
    args = parse_args(arguments)
    smooth_plan = None
    if args.algorithm.startswith('RRT'):
        plan, problem, _ = rrt.test_rrt_env(
                num_samples=args.num_samples,
                step_length=args.step_length,
                env=args.env,
                connect=(args.algorithm.endswith('connect')),
                connect_prob=args.bias,
                bidirectional=('-bi' in args.algorithm),
                constraints=args.constraints,
                )
        Qs, edges = problem.T.get_states_and_edges()
        if args.smooth:
            smooth_plan = problem.smooth_path(plan)
    elif args.algorithm.startswith('PRM'):
        plan, problem, _ = prm.test_prm_env(
                num_samples=args.num_samples,
                step_length=args.step_length,
                env=args.env,
                use_rrt=args.algorithm.endswith('RRT'),
                rrt_connect_prob=args.bias,
                use_gaussian_sampling=('gauss' in args.algorithm),
                )
        Qs, edges = problem.get_full_graph()
    else:
        print >> sys.stderr, 'Error: unsupported algorithm supplied:', args.algorithm
        sys.exit(1)
    env = collisions.PolygonEnvironment()
    env.read_env(args.env)
    #env.draw_env()
    print 'planner:', problem.__dict__
    if args.plot_graph:
        graph_image = None
        if args.output_image is not None:
            base, ext = os.path.splitext(args.output_image)
            graph_image = base + '_graph' + ext
        env.draw_plan(None, Qs, edges, show=args.display,
                output_filename=graph_image, dynamic_tree=args.display)
    env.draw_plan(plan, Qs, edges, show=args.display,
            output_filename=args.output_image, dynamic_tree=args.display)
    if smooth_plan is not None:
        smooth_Qs = np.array(smooth_plan)
        smooth_edges = [(smooth_plan[i], smooth_plan[i+1]) for i in range(len(smooth_plan)-1)]
        if args.plot_graph:
            smooth_graph_image = None
            if args.output_image is not None:
                base, ext = os.path.splitext(args.output_image)
                smooth_graph_image = base + '_smooth_graph' + ext
            env.draw_plan(None, smooth_Qs, smooth_edges, show=args.display,
                    output_filename=smooth_graph_image, dynamic_tree=args.display)
        smooth_outfile = None
        if args.output_image is not None:
            base, ext = os.path.splitext(args.output_image)
            smooth_outfile = base + '_smooth' + ext
        env.draw_plan(
                smooth_plan,
                smooth_Qs,
                smooth_edges,
                show=args.display,
                output_filename=smooth_outfile,
                dynamic_tree=args.display
                )

if __name__ == '__main__':
    main(sys.argv[1:])
