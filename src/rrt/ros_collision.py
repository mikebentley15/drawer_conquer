import os
import rospy
from drawer_conquer.srv import \
    LoadModel, \
    CheckCollision
from drawer_conquer.msg import \
    BaxterConfiguration, \
    NamedTransform, \
    Model3D
import baxter_interface

class RosCollision(object):
    def __init__(self):
        self.detectors = {}
        self.all_models = (
            'brick',
            'drawer_inside_handle',
            'drawer_inside_hollow',
            'drawer_outside',
            'table',
            )
        self.model_filenames = {
            'brick' : 'brick.fcl',
            'drawer_inside_handle' : 'drawer_from_outside.fcl',
            'drawer_inside_hollow' : 'drawer_from_inside.fcl',
            'drawer_outside' : 'drawer_case.fcl',
            'table' : 'table.fcl',
            }
        for name, filename in self.model_filenames.items():
            self.load_model(name, filename)
        self.current_models = []
        self.current_arm = None
        self.left = baxter_interface.Limb('left')
        self.right = baxter_interface.Limb('right')
        self.lgrip = baxter_interface.Gripper('left')
        self.rgrip = baxter_interface.Gripper('right')
        self.lgrip_position = self.lgrip.position() / 100.0
        self.rgrip_position = self.rgrip.position() / 100.0

    def load_model(self, name, filename):
        if os.path.isfile(filename):
            modelfile = filename
        else:
            scriptdir = os.path.abspath(os.path.dirname(__file__))
            modeldir = os.path.join(scriptdir, '..', '..', 'models')
            modeldir = os.path.abspath(modeldir)
            modelfile = os.path.join(modeldir, filename)
        service_name = 'load_model'
        rospy.wait_for_service(service_name)
        try:
            load_model = rospy.ServiceProxy(service_name, LoadModel)
            response = load_model(name, modelfile)
            print 'Request: ', (name, modelfile)
            print 'Response:', response
        except rospy.ServiceException, e:
            print 'Service call load_model failed:', e

    def set_important_models(self, model_list):
        self.current_models[:] = model_list

    def set_current_arm(self, arm):
        assert arm in ['left', 'right']
        self.current_arm = arm

    def add_pose_detector(self, model_name, pose_detector):
        assert model_name in self.all_models
        self.detectors[model_name] = pose_detector

    def check_collision(self, config_array):
        if self.current_arm == 'left':
            left_config = list(config_array)
            right_angles = self.right.joint_angles()
            right_config = [
                right_angles['right_s0'],
                right_angles['right_s1'],
                right_angles['right_e0'],
                right_angles['right_e1'],
                right_angles['right_w0'],
                right_angles['right_w1'],
                right_angles['right_w2'],
                ]
        elif self.current_arm == 'right':
            right_config = list(config_array)
            left_angles = self.left.joint_angles()
            left_config = [
                left_angles['left_s0'],
                left_angles['left_s1'],
                left_angles['left_e0'],
                left_angles['left_e1'],
                left_angles['left_w0'],
                left_angles['left_w1'],
                left_angles['left_w2'],
                ]
        else:
            raise RuntimeError('Need to set a current arm before collision checking')

        # TODO may need to update gripper position, this was moved to init
        #left_config.append(self.lgrip_position)
        #right_config.append(self.rgrip_position)
        left_config.append(self.lgrip.position() / 100.0)
        right_config.append(self.rgrip.position() / 100.0)

        config = BaxterConfiguration()
        config.left.s0 = left_config[0]
        config.left.s1 = left_config[1]
        config.left.e0 = left_config[2]
        config.left.e1 = left_config[3]
        config.left.w0 = left_config[4]
        config.left.w1 = left_config[5]
        config.left.w2 = left_config[6]
        config.left.gripper_open_percentage = left_config[7]
        config.right.s0 = right_config[0]
        config.right.s1 = right_config[1]
        config.right.e0 = right_config[2]
        config.right.e1 = right_config[3]
        config.right.w0 = right_config[4]
        config.right.w1 = right_config[5]
        config.right.w2 = right_config[6]
        config.right.gripper_open_percentage = right_config[7]

        model_transforms = [NamedTransform(x, self.detectors[x].pose)
                            for x in self.current_models]

        service_name = 'check_collision'
        rospy.wait_for_service(service_name)
        try:
            print 'Checking collision:'
            print model_transforms
            print config
            check_collision = rospy.ServiceProxy(service_name, CheckCollision)
            response = check_collision(model_transforms, config)
            print 'Response:', response
        except rospy.ServiceException, e:
            print 'Service call check_collision failed:', e
        return response.models_in_collision != []
