#ifndef FCL_HELPER_H
#define FCL_HELPER_H

#include "drawer_conquer/Model3D.h"

//#include <fcl/common/types.h>
//#include <fcl/math/triangle.h>
//#include <fcl/data_types.h> // for type Triangle

#include <fcl/fcl.h>

#include <vector>

/** Loads an fcl file into vectors and triangles
 *
 * This function was copied verbatim from the fcl git repository from
 *   fcl/tests/test_fcl_utility.h
 * In that file, this function was called loadOBJFile()
 *
 * @param[in] filename - file path containing the model
 * @param[out] points - vertices of the model
 * @param[out] triangles - indices into points for model faces
 * @return True if it succeeded
 */
bool loadFclFile(
    const char* filename,
    std::vector<fcl::Vector3d>& points,
    std::vector<fcl::Triangle>& triangles);

bool model3DToFcl(
    const drawer_conquer::Model3D &model,
    std::vector<fcl::Vector3d>& points,
    std::vector<fcl::Triangle>& triangles);

drawer_conquer::Model3D fclToModel3D(
    const std::vector<fcl::Vector3d>& points,
    const std::vector<fcl::Triangle>& triangles);

#endif // FCL_HELPER_H
