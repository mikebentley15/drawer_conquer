#include "CollisionChecker.h"
#include "fcl_helper.h"

// ROS services
#include "drawer_conquer/AddModel.h"
#include "drawer_conquer/CheckCollision.h"
#include "drawer_conquer/LoadModel.h"

#include "ros/ros.h"

#include <string>
#include <cstdint>

// Aliases for ease of typing
using drawer_conquer::AddModel;
using AMReq = AddModel::Request;
using AMRes = AddModel::Response;
using drawer_conquer::CheckCollision;
using CCReq = CheckCollision::Request;
using CCRes = CheckCollision::Response;
using drawer_conquer::LoadModel;
using LMReq = LoadModel::Request;
using LMRes = LoadModel::Response;

// Global checker object.  Initialize in main()
std::unique_ptr<CollisionChecker> checker = nullptr;


/** Callback for the add_model service
 *
 * This function simply adds the given model with the associated name.  If the
 * name already exists on this service, then it will be overwritten.
 *
 * @param req - Contains fields
 *   name - a string for the name of the model
 *   model - a Model3D object that contains the model itself
 * @param res - Contains fields
 *   ok - True meaning it was added or overwritten successfully
 * @return True if it was added or overwritten successfully
 */
bool add_model(AMReq &req, AMRes &res) {
  ROS_INFO("Add model requested for model %s", req.name.c_str());

  std::vector<fcl::Vector3d> vertices;
  std::vector<fcl::Triangle> triangles;
  res.ok = model3DToFcl(req.model, vertices, triangles);

  if (res.ok) {
    res.ok = checker->add_model(req.name, vertices, triangles);
  }

  return res.ok;
}

/** Callback for the load_model service
 *
 * This function will load the given filepath with the associated name.  If the
 * name already exists on this service, then it will be overwritten.
 *
 * @param[in] req - Contains fields
 *   name - a string for the name of the model
 *   abs_filepath - filepath to the desired model content
 * @param[out] res - Contains fields
 *   ok - True meaning it was added or overwritten successfully
 * @return True if it was added or overwritten successfully.
 */
bool load_model(LMReq &req, LMRes &res) {
  ROS_INFO("Load model requested for model %s at %s",
      req.name.c_str(), req.abs_filepath.c_str());

  std::vector<fcl::Vector3d> vertices;
  std::vector<fcl::Triangle> triangles;

  res.ok = loadFclFile(req.abs_filepath.c_str(), vertices, triangles);

  if (res.ok) {
    checker->add_model(req.name, vertices, triangles);
  } else {
    ROS_ERROR("Could not load model %s from %s",
        req.name.c_str(), req.abs_filepath.c_str());
  }

  return res.ok;
}

/** Callback for the check_collision service
 *
 * This function checks for collisions between the models specified in the
 * request.  Only the models specified in the request will be considered when
 * doing collision detection.  In response, the names of the objects in
 * collision will be returned.
 *
 * @param[in] req - Contains fields
 *   model_transforms - an array of NamedTransform objects
 *   config - a BaxterConfiguration object
 * @param[out] res - Contains fields
 *   models_in_collision - an array of names for models that are in collision
 * @return True if the check completes successfully.  Will be false if the
 *   referenced models in the request do not exist in this service.
 */
bool check_collision(CCReq &req, CCRes &res) {
  std::string model_names;
  for (auto transform : req.model_transforms) {
    if (model_names.length() > 0) {
      model_names += ", ";
    }
    model_names += transform.name;
  }
  ROS_INFO("Check collision requested for models [%s]", model_names.c_str());

  res.models_in_collision =
    checker->check_collision(req.model_transforms, req.config);

  model_names = "";
  for (auto name : res.models_in_collision) {
    if (model_names.length() > 0) {
      model_names += ", ";
    }
    model_names += name;
  }
  ROS_INFO("Check collision finished.  Models in collision: [%s]",
      model_names.c_str());
  return true;
}

/** Main entry point.  Starts the ROS services */
int main(int arg_count, char* arg_list[]) {
  ros::init(arg_count, arg_list, "collision_server");
  ros::NodeHandle n;

  // Initialize the global collision checker
  checker.reset(new CollisionChecker());

  // Register the services
  ros::ServiceServer add_model_service =
    n.advertiseService("add_model", add_model);
  ROS_INFO("Service add_model is ready");
  ros::ServiceServer load_model_service =
    n.advertiseService("load_model", load_model);
  ROS_INFO("Service load_model is ready");
  ros::ServiceServer check_service =
    n.advertiseService("check_collision", check_collision);
  ROS_INFO("Service check_collision is ready");
  ROS_INFO("Server collision_server is done initializing");
  ros::spin();

  // Clean up the checker.  This will call its destructor.
  checker.reset(nullptr);

  return 0;
}
