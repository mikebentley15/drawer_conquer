#ifndef DC_HELPER_H
#define DC_HELPER_H

#include "drawer_conquer/BaxterConfiguration.h"
#include "drawer_conquer/BaxterTransforms.h"

drawer_conquer::BaxterTransforms get_baxter_transforms(
    const drawer_conquer::BaxterConfiguration &config);

#endif // DC_HELPER_H
