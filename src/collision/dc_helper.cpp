#include "drawer_conquer/BaxterConfiguration.h"
#include "drawer_conquer/BaxterTransforms.h"
#include "drawer_conquer/FindBaxterTransform.h"

#include "ros/ros.h"

#include <stdexcept>

using drawer_conquer::BaxterConfiguration;
using drawer_conquer::BaxterTransforms;
using drawer_conquer::FindBaxterTransform;

BaxterTransforms get_baxter_transforms(const BaxterConfiguration &config) {
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<FindBaxterTransform>("find_baxter_transform");
  FindBaxterTransform srv;
  srv.request.config = config;
  ROS_INFO("Calling ROS Service find_baxter_transform");
  if (client.call(srv)) {
    ROS_INFO("Succeeded");
    return srv.response.transforms;
  } else {
    ROS_ERROR("Failed to call service find_baxter_transform");
    throw std::runtime_error("Failed to call service find_baxter_transform");
  }
}


