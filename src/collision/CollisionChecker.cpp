#include "CollisionChecker.h"
#include "dc_helper.h"

#include <geometry_msgs/Transform.h>

#include <ros/ros.h>

#include <fcl/common/types.h>
#include <fcl/narrowphase/collision.h>
#include <fcl/narrowphase/collision_request.h>
#include <fcl/narrowphase/collision_result.h>
#include <fcl/narrowphase/detail/traversal/collision/mesh_shape_collision_traversal_node.h>

#include <tuple>
#include <vector>

using drawer_conquer::BaxterConfiguration;
using drawer_conquer::NamedTransform;
using fcl::BVHModel;
using fcl::Boxd;
using fcl::CollisionGeometry;
using fcl::CollisionRequest;
using fcl::CollisionResult;
using fcl::Cylinderd;
using fcl::Quaterniond;
using fcl::Transform3d;
using fcl::Vector3d;
using fcl::detail::BVSplitter;

namespace {
  // Define method for doing collision detection here
  using BV = fcl::AABB<double>;
  auto split_method = fcl::detail::SPLIT_METHOD_BV_CENTER;

  std::string to_string(const Cylinderd &c) {
    std::string str("Cylinderd(");
    str += std::to_string(c.radius);
    str += ", ";
    str += std::to_string(c.lz);
    str += ")";
    return str;
  }

  std::string to_string(const Boxd &b) {
    std::string str("Boxd(");
    str += std::to_string(b.side[0]);
    str += ", ";
    str += std::to_string(b.side[1]);
    str += ", ";
    str += std::to_string(b.side[2]);
    str += ")";
    return str;
  }

  std::string to_string(const Transform3d &t) {
    std::string str("Transform3d(\n");
    for (int i = 0; i < 3; i++) {
      str += "  [";
      for (int j = 0; j < 3; j++) {
        str += std::to_string(t(i, j));
        str += ", ";
      }
      str += std::to_string(t(i, 3));
      str += "]\n";
    }
    str += ")";
    return str;
  }

  std::string to_string(const CollisionGeometry<double> &g) {
    auto b = static_cast<const BVHModel<BV> &>(g);
    std::string str("BVHModel(\n");
    for (auto i = 0; i < b.num_vertices; i++) {
      str += "  v ";
      str += std::to_string(b.vertices[i][0]);
      str += ", ";
      str += std::to_string(b.vertices[i][1]);
      str += ", ";
      str += std::to_string(b.vertices[i][2]);
      str += "\n";
    }
    for (auto i = 0; i < b.num_tris; i++) {
      str += "  f ";
      str += std::to_string(b.tri_indices[i][0]);
      str += ", ";
      str += std::to_string(b.tri_indices[i][1]);
      str += ", ";
      str += std::to_string(b.tri_indices[i][2]);
      str += "\n";
    }
    str += ")";
    return str;
  }

  std::string to_string(const Quaterniond &q) {
    std::string str("Quaterniond(");
    str += std::to_string(q.x());
    str += ", ";
    str += std::to_string(q.y());
    str += ", ";
    str += std::to_string(q.z());
    str += ", ";
    str += std::to_string(q.w());
    str += ")";
    return str;
  }

  std::string to_string(const Vector3d &v) {
    std::string str("Vector3d(");
    str += std::to_string(v[0]);
    str += ", ";
    str += std::to_string(v[1]);
    str += ", ";
    str += std::to_string(v[2]);
    str += ")";
    return str;
  }

  template <typename O1, typename O2>
  bool check_collision_impl(
      const O1 &m1,
      const Transform3d &pose1,
      const O2 &m2,
      const Transform3d &pose2)
  {
    CollisionRequest<double> request;
    CollisionResult<double> result;
    bool is_in_collision;

    result.clear();
    auto num_in_collision = fcl::collide(&m1, pose1, &m2, pose2, request, result);
    //ROS_INFO("num_in_collision = %ld", num_in_collision);
    is_in_collision = num_in_collision > 0;
    if (is_in_collision) {
      ROS_INFO("Collision Detected");
      ROS_INFO("  Object 1: %s", to_string(m1).c_str());
      ROS_INFO("  Object 2: %s", to_string(m2).c_str());
      ROS_INFO("  Trans 1:  %s", to_string(pose1).c_str());
      ROS_INFO("  Trans 2:  %s", to_string(pose2).c_str());
    }
    return is_in_collision;
  }
}


CollisionChecker::CollisionChecker()
  : m_object_map()
  , m_arm_base(0.06, 0.2722)
  , m_s0(0.06, 0.12)
  , m_s1(0.06, 0.364)
  , m_e0(0.06, 0.1)
  , m_e1(0.06, 0.378)
  , m_w0(0.06, 0.1)
  , m_w1(0.06, 0.28)
  , m_w2(0.04, 0.0464)
  , m_gripper_finger(0.0155, 0.0125, 0.0734)
{
  ROS_INFO("Initialized m_arm_base:       %s", to_string(m_arm_base).c_str());
  ROS_INFO("Initialized m_s0:             %s", to_string(m_s0).c_str());
  ROS_INFO("Initialized m_s1:             %s", to_string(m_s1).c_str());
  ROS_INFO("Initialized m_e0:             %s", to_string(m_e0).c_str());
  ROS_INFO("Initialized m_e1:             %s", to_string(m_e1).c_str());
  ROS_INFO("Initialized m_w0:             %s", to_string(m_w0).c_str());
  ROS_INFO("Initialized m_w1:             %s", to_string(m_w1).c_str());
  ROS_INFO("Initialized m_w2:             %s", to_string(m_w2).c_str());
  ROS_INFO("Initialized m_gripper_finger: %s", to_string(m_gripper_finger).c_str());
}

bool CollisionChecker::add_model(
    const std::string& name,
    const std::vector<fcl::Vector3d> &vertices,
    const std::vector<fcl::Triangle> &triangles)
{

  std::unique_ptr<BVHModel<BV>> model(new BVHModel<BV>); 
  model->bv_splitter.reset(new BVSplitter<BV>(split_method));

  model->beginModel();
  model->addSubModel(vertices, triangles);
  model->endModel();

  if (m_object_map.count(name) == 0) {
    m_object_map.insert({name, std::shared_ptr<CollisionGeometry<double>>(std::move(model))});
  } else {
    m_object_map[name].reset(model.release());
  }

  return true;
}

std::vector<std::string> CollisionChecker::check_collision(
    const std::vector<NamedTransform> &transforms,
    const BaxterConfiguration &config)
{
  auto baxter_transforms = get_baxter_transforms(config);

  auto trans_to_pose = [](geometry_msgs::Transform trans, double zshift) {
    // The zshift is because robot links have transforms specified that are
    // from the proximal end, but FCL expects it to be from the center of the
    // link.  Here we do a zshift in the z-direction of the link.
    Quaterniond quat(trans.rotation.x, trans.rotation.y, trans.rotation.z,
        trans.rotation.w);
    Vector3d translation(trans.translation.x, trans.translation.y, trans.translation.z);
    Transform3d newtrans;
    newtrans.setIdentity();
    newtrans.translate(translation);
    newtrans.rotate(quat);
    Vector3d zshift_translation(0, 0, zshift);
    Transform3d zshift_trans;
    zshift_trans.setIdentity();
    zshift_trans.translate(zshift_translation);
    ROS_INFO("rotation:     %s", to_string(quat).c_str());
    ROS_INFO("translation:  %s", to_string(translation).c_str());
    ROS_INFO("before trans: %s", to_string(newtrans).c_str());
    ROS_INFO("shift trans:  %s", to_string(zshift_trans).c_str());
    ROS_INFO("after trans:  %s", to_string(newtrans * zshift_trans).c_str());
    return newtrans * zshift_trans;
    //return newtrans;
  };

  ROS_INFO("left.to_w2.x = %lf", baxter_transforms.left.to_w2.translation.x);
  ROS_INFO("left.to_w2.y = %lf", baxter_transforms.left.to_w2.translation.y);
  ROS_INFO("left.to_w2.z = %lf", baxter_transforms.left.to_w2.translation.z);
  ROS_INFO("left.to_e0.x = %lf", baxter_transforms.left.to_e0.translation.x);
  ROS_INFO("left.to_e0.y = %lf", baxter_transforms.left.to_e0.translation.y);
  ROS_INFO("left.to_e0.z = %lf", baxter_transforms.left.to_e0.translation.z);
  ROS_INFO("right.to_s1.x = %lf", baxter_transforms.right.to_s1.translation.x);
  ROS_INFO("right.to_s1.y = %lf", baxter_transforms.right.to_s1.translation.y);
  ROS_INFO("right.to_s1.z = %lf", baxter_transforms.right.to_s1.translation.z);

  // Make some vectors for ease of checking all joints
  std::vector<std::tuple<std::string, Cylinderd, Transform3d>> left_arm;
  left_arm.emplace_back("left.s1", m_s1, trans_to_pose(baxter_transforms.left.to_s1, m_s1.lz/2.));
  left_arm.emplace_back("left.e0", m_e0, trans_to_pose(baxter_transforms.left.to_e0, m_e0.lz/2.));
  left_arm.emplace_back("left.e1", m_e1, trans_to_pose(baxter_transforms.left.to_e1, m_e1.lz/2.));
  left_arm.emplace_back("left.w0", m_w0, trans_to_pose(baxter_transforms.left.to_w0, m_w0.lz/2.));
  left_arm.emplace_back("left.w1", m_w1, trans_to_pose(baxter_transforms.left.to_w1, m_w1.lz/2.));
  left_arm.emplace_back("left.w2", m_w2, trans_to_pose(baxter_transforms.left.to_w2, m_w2.lz/2.));

  std::vector<std::tuple<std::string, Boxd, Transform3d>> left_gripper;
  left_gripper.emplace_back("left.gripper.finger1", m_gripper_finger,
      trans_to_pose(baxter_transforms.left.to_gripper1, m_gripper_finger.side[2]/2.));
  left_gripper.emplace_back("left.gripper.finder2", m_gripper_finger,
      trans_to_pose(baxter_transforms.left.to_gripper2, m_gripper_finger.side[2]/2.));

  decltype(left_arm) right_arm;
  right_arm.emplace_back("right.s1", m_s1, trans_to_pose(baxter_transforms.right.to_s1, m_s1.lz/2.));
  right_arm.emplace_back("right.e0", m_e0, trans_to_pose(baxter_transforms.right.to_e0, m_e0.lz/2.));
  right_arm.emplace_back("right.e1", m_e1, trans_to_pose(baxter_transforms.right.to_e1, m_e1.lz/2.));
  right_arm.emplace_back("right.w0", m_w0, trans_to_pose(baxter_transforms.right.to_w0, m_w0.lz/2.));
  right_arm.emplace_back("right.w1", m_w1, trans_to_pose(baxter_transforms.right.to_w1, m_w1.lz/2.));
  right_arm.emplace_back("right.w2", m_w2, trans_to_pose(baxter_transforms.right.to_w2, m_w2.lz/2.));

  decltype(left_gripper) right_gripper;
  right_gripper.emplace_back("right.gripper.finger1", m_gripper_finger,
      trans_to_pose(baxter_transforms.right.to_gripper1, m_gripper_finger.side[2]/2.));
  right_gripper.emplace_back("right.gripper.finder2", m_gripper_finger,
      trans_to_pose(baxter_transforms.right.to_gripper2, m_gripper_finger.side[2]/2.));

  decltype(left_arm) both_arms;
  both_arms.insert(both_arms.end(), left_arm.begin(), left_arm.end());
  both_arms.insert(both_arms.end(), right_arm.begin(), right_arm.end());

  decltype(left_gripper) both_grippers;
  both_grippers.insert(both_grippers.end(), left_gripper.begin(), left_gripper.end());
  both_grippers.insert(both_grippers.end(), right_gripper.begin(), right_gripper.end());

  //
  // Check collision between left and right arm and grippers
  //

  for (auto left_link : left_arm) {
    for (auto right_link : right_arm) {
      ROS_INFO("Checking %s and %s", std::get<0>(left_link).c_str(), std::get<0>(right_link).c_str());
      if (check_collision_impl(std::get<1>(left_link),  std::get<2>(left_link),
                               std::get<1>(right_link), std::get<2>(right_link)))
      {
        return {std::get<0>(left_link), std::get<0>(right_link)};
      }
    }
    for (auto right_finger : right_gripper) {
      ROS_INFO("Checking %s and %s", std::get<0>(left_link).c_str(), std::get<0>(right_finger).c_str());
      if (check_collision_impl(std::get<1>(left_link),    std::get<2>(left_link),
                               std::get<1>(right_finger), std::get<2>(right_finger)))
      {
        return {std::get<0>(left_link), std::get<0>(right_finger)};
      }
    }
  }

  for (auto left_finger : left_gripper) {
    for (auto right_link : right_arm) {
      ROS_INFO("Checking %s and %s", std::get<0>(left_finger).c_str(), std::get<0>(right_link).c_str());
      if (check_collision_impl(std::get<1>(left_finger), std::get<2>(left_finger),
                               std::get<1>(right_link),  std::get<2>(right_link)))
      {
        return {std::get<0>(left_finger), std::get<0>(right_link)};
      }
    }
    for (auto right_finger : right_gripper) {
      ROS_INFO("Checking %s and %s", std::get<0>(left_finger).c_str(), std::get<0>(right_finger).c_str());
      if (check_collision_impl(std::get<1>(left_finger),  std::get<2>(left_finger),
                               std::get<1>(right_finger), std::get<2>(right_finger)))
      {
        return {std::get<0>(left_finger), std::get<0>(right_finger)};
      }
    }
  }


  //
  // Check collision between objects and arms
  //

  for (auto trans : transforms) {
    auto model = m_object_map[trans.name];
    auto pose = trans_to_pose(trans.transform, 0);

    for (auto link : both_arms) {
      ROS_INFO("Checking %s and %s", std::get<0>(link).c_str(), trans.name.c_str());
      if (check_collision_impl(*model, pose, std::get<1>(link), std::get<2>(link))) {
        return {trans.name, std::get<0>(link)};
      }
    }
    for (auto finger : both_grippers) {
      ROS_INFO("Checking %s and %s", std::get<0>(finger).c_str(), trans.name.c_str());
      if (check_collision_impl(*model, pose, std::get<1>(finger), std::get<2>(finger))) {
        return {trans.name, std::get<0>(finger)};
      }
    }
  }

  return {};
}
