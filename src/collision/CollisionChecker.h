#ifndef COLLISION_CHECKER_H
#define COLLISION_CHECKER_H

#include "drawer_conquer/BaxterConfiguration.h"
#include "drawer_conquer/NamedTransform.h"

#include <fcl/common/types.h>
#include <fcl/geometry/collision_geometry.h>
#include <fcl/geometry/shape/box.h>
#include <fcl/geometry/shape/cylinder.h>
#include <fcl/math/triangle.h>
//#include <fcl/fcl.h>

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

/** Class for performing collision checks */
class CollisionChecker {
public:
  CollisionChecker();

  // Disable copies
  CollisionChecker(const CollisionChecker& other) = delete;
  CollisionChecker& operator=(const CollisionChecker& other) = delete;

  bool add_model(
      const std::string& name,
      const std::vector<fcl::Vector3d> &vertices,
      const std::vector<fcl::Triangle> &triangles);

  std::vector<std::string> check_collision(
      const std::vector<drawer_conquer::NamedTransform> &transforms,
      const drawer_conquer::BaxterConfiguration &config
      );

private:
  std::unordered_map<std::string, std::shared_ptr<fcl::CollisionGeometry<double>>> m_object_map;
  fcl::Cylinderd m_arm_base;
  fcl::Cylinderd m_s0;
  fcl::Cylinderd m_s1;
  fcl::Cylinderd m_e0;
  fcl::Cylinderd m_e1;
  fcl::Cylinderd m_w0;
  fcl::Cylinderd m_w1;
  fcl::Cylinderd m_w2;
  fcl::Boxd m_gripper_finger;
};

#endif // COLLISION_CHECKER_H
