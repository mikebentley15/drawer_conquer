#include "fcl_helper.h"

#include <iostream>

#include <cstdlib>
#include <cstdio>
#include <cstring>

bool loadFclFile(
    const char* filename,
    std::vector<fcl::Vector3d>& points,
    std::vector<fcl::Triangle>& triangles)
{
  FILE* file = fopen(filename, "rb");
  if(!file)
  {
    std::cerr << "file not exist" << std::endl;
    return false;
  }

  bool has_normal = false;
  bool has_texture = false;
  char line_buffer[2000];
  while(fgets(line_buffer, 2000, file))
  {
    char* first_token = strtok(line_buffer, "\r\n\t ");
    if(!first_token || first_token[0] == '#' || first_token[0] == 0)
      continue;

    switch(first_token[0])
    {
    case 'v':
      {
        if(first_token[1] == 'n')
        {
          strtok(nullptr, "\t ");
          strtok(nullptr, "\t ");
          strtok(nullptr, "\t ");
          has_normal = true;
        }
        else if(first_token[1] == 't')
        {
          strtok(nullptr, "\t ");
          strtok(nullptr, "\t ");
          has_texture = true;
        }
        else
        {
          double x = atof(strtok(nullptr, "\t "));
          double y = atof(strtok(nullptr, "\t "));
          double z = atof(strtok(nullptr, "\t "));
          points.emplace_back(x, y, z);
        }
      }
      break;
    case 'f':
      {
        fcl::Triangle tri;
        char* data[30];
        int n = 0;
        while((data[n] = strtok(nullptr, "\t \r\n")) != nullptr)
        {
          if(strlen(data[n]))
            n++;
        }

        for(int t = 0; t < (n - 2); ++t)
        {
          if((!has_texture) && (!has_normal))
          {
            tri[0] = atoi(data[0]) - 1;
            tri[1] = atoi(data[1]) - 1;
            tri[2] = atoi(data[2]) - 1;
          }
          else
          {
            const char *v1;
            for(int i = 0; i < 3; i++)
            {
              // vertex ID
              if(i == 0)
                v1 = data[0];
              else
                v1 = data[t + i];

              tri[i] = atoi(v1) - 1;
            }
          }
          triangles.push_back(tri);
        }
      }
    }
  }
  return true;
}

bool model3DToFcl(
    const drawer_conquer::Model3D &model,
    std::vector<fcl::Vector3d>& vertices,
    std::vector<fcl::Triangle>& triangles)
{
  bool is_error = false;
  for (auto v : model.vertices) {
    vertices.emplace_back(v.x, v.y, v.z);
  }
  for (auto f : model.faces) {
    triangles.emplace_back(f.v1, f.v2, f.v3);
    is_error = is_error || f.v1 < 0 || f.v1 >= vertices.size();
    is_error = is_error || f.v2 < 0 || f.v2 >= vertices.size();
    is_error = is_error || f.v3 < 0 || f.v3 >= vertices.size();
  }
  return !is_error;
}

drawer_conquer::Model3D fclToModel3D(
    const std::vector<fcl::Vector3d>& points,
    const std::vector<fcl::Triangle>& triangles)
{
  drawer_conquer::Model3D model;
  for (auto p : points) {
    geometry_msgs::Vector3 vec;
    vec.x = p[0];
    vec.y = p[1];
    vec.z = p[2];
    model.vertices.push_back(vec);
  }
  for (auto t : triangles) {
    drawer_conquer::SurfaceFace face;
    face.v1 = t[0];
    face.v2 = t[1];
    face.v3 = t[2];
    model.faces.push_back(face);
  }
  return model;
}

