#!/usr/bin/env python
import argparse
import math
import random
import struct
import sys
import copy

import rospy
import rospkg
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)

from std_msgs.msg import (
    Header,
    Empty,
)

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)
from baxter_interface import CHECK_VERSION

import baxter_interface


class InverseKinematics(object):
    def __init__(self, verbose = True):
        self._hover_distance = 0.15
        self._limb_name = 'left'
        self.left_arm = baxter_interface.limb.Limb("left")
        self.right_arm = baxter_interface.limb.Limb("right")
        self.left_gripper = baxter_interface.Gripper('left')
        self.right_gripper = baxter_interface.Gripper('right')
        self._left_joint_names = self.left_arm.joint_names()
        self._right_joint_names = self.right_arm.joint_names()
        self._rate = 500.0  # Hz
        self._verbose = verbose  # bool

        if self.left_gripper.type() != 'custom':
            if not (self.left_gripper.calibrated() or
                            self.left_gripper.calibrate() == True):
                rospy.logerr("%s (%s) calibration failed.",
                             self.left_gripper.name.capitalize(),
                             self.left_gripper.type())
                sys.exit(1)

        # if self.right_gripper.type() != 'custom':
        #     if not (self.right_gripper.calibrated() or
        #                     self.left_gripper.calibrate() == True):
        #         rospy.logerr("%s (%s) calibration failed.",
        #                      self.right_gripper.name.capitalize(),
        #                      self.right_gripper.type())
        #         sys.exit(1)


        ns_left = "ExternalTools/" + 'left' + "/PositionKinematicsNode/IKService"
        self.left_iksvc = rospy.ServiceProxy(ns_left, SolvePositionIK)
        rospy.wait_for_service(ns_left, 5.0)

        ns_right = "ExternalTools/" + 'right' + "/PositionKinematicsNode/IKService"
        self.right_iksvc = rospy.ServiceProxy(ns_right, SolvePositionIK)

        print("Getting robot state... ")
        self._rs = baxter_interface.RobotEnable(CHECK_VERSION)
        self._init_state = self._rs.state().enabled
        print("Enabling robot... ")
        self._rs.enable()

        # set joint state publishing to 500Hz

    def ik_request(self, pose, ik_solver):
        hdr = Header(stamp=rospy.Time.now(), frame_id='base')
        ikreq = SolvePositionIKRequest()
        ikreq.pose_stamp.append(PoseStamped(header=hdr, pose=pose))
        try:
            resp = ik_solver(ikreq)
        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
            return False
        # Check if result valid, and type of seed ultimately used to get solution
        # convert rospy's string representation of uint8[]'s to int's
        resp_seeds = struct.unpack('<%dB' % len(resp.result_type), resp.result_type)
        limb_joints = {}
        if (resp_seeds[0] != resp.RESULT_INVALID):
            seed_str = {
                        ikreq.SEED_USER: 'User Provided Seed',
                        ikreq.SEED_CURRENT: 'Current Joint Angles',
                        ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                       }.get(resp_seeds[0], 'None')
            if self._verbose:
                print("IK Solution SUCCESS - Valid Joint Solution Found from Seed Type: {0}".format(
                         (seed_str)))
            # Format solution into Limb API-compatible dictionary
            limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))
            if self._verbose:
                print("IK Joint Solution:\n{0}".format(limb_joints))
                print("------------------")
        else:
            rospy.logerr("INVALID POSE - No Valid Joint Solution Found.")
            return False
        return limb_joints




    def move_to_start(self, start_angles=None):
        print("Moving the {0} arm to start pose...".format(self._limb_name))
        if not start_angles:
            start_angles = dict(zip(self._joint_names, [0]*7))
        self._guarded_move_to_joint_position(start_angles)
        self.left_gripper_open()
        rospy.sleep(1.0)
        print("Running. Ctrl-c to quit")



    def left_gripper_open(self):
        self.left_gripper.open()
        rospy.sleep(1.0)

    def left_gripper_close(self):
        self.left_gripper.close()
        rospy.sleep(1.0)

    def pick(self, pose):
        # open the gripper
        self.left_gripper_open()
        # servo above pose
        self._approach(pose)
        # servo to pose
        self._servo_to_pose(pose)
        # close gripper
        self.left_gripper_close()
        # retract to clear object
        self._retract()

    def _servo_to_pose(self, pose):
        # servo down to release
        joint_angles = self.ik_request(pose,self.left_iksvc)
        self._guarded_move_to_joint_position(joint_angles)


    def _retract(self):
        # retrieve current pose from endpoint
        current_pose = self.left_arm.endpoint_pose()
        ik_pose = Pose()
        ik_pose.position.x = current_pose['position'].x
        ik_pose.position.y = current_pose['position'].y
        ik_pose.position.z = current_pose['position'].z + self._hover_distance
        ik_pose.orientation.x = current_pose['orientation'].x
        ik_pose.orientation.y = current_pose['orientation'].y
        ik_pose.orientation.z = current_pose['orientation'].z
        ik_pose.orientation.w = current_pose['orientation'].w
        joint_angles = self.ik_request(ik_pose,self.left_iksvc)
        # servo up from current pose
        self._guarded_move_to_joint_position(joint_angles)

    def place(self, pose):
        # servo above pose
        self._approach(pose)
        # servo to pose
        self._servo_to_pose(pose)
        # open the gripper
        self.left_gripper_open()
        # retract to clear object
        self._retract()

    def _approach(self, pose):
        approach = copy.deepcopy(pose)
        # approach with a pose the hover-distance above the requested pose
        approach.position.z = approach.position.z + self._hover_distance
        joint_angles = self.ik_request(approach,self.left_iksvc)
        self._guarded_move_to_joint_position(joint_angles)

    #move the left arm to the specific position
    def _guarded_move_to_joint_position(self, joint_angles):
        if joint_angles:
            self.left_arm.move_to_joint_positions(joint_angles)
        else:
            rospy.logerr("No Joint Angles provided for move_to_joint_positions. Staying put.")


    #how to shut down the robot
    def reset_control_modes(self):
        rate = rospy.Rate(int(self._rate))
        for _ in xrange(100):
            if rospy.is_shutdown():
                return False
            self.left_arm.exit_control_mode()
            self.right_arm.exit_control_mode()
            #self._pub_rate.publish(100)  # 100Hz default joint state rate
            rate.sleep()

    def set_neutral(self):
        """
        Sets both arms back into a neutral pose.
        """
        print("Moving to neutral pose...")
        self.left_arm.move_to_neutral()
        self.right_arm.move_to_neutral()

    def clean_shutdown(self):
        print("\nExiting example...")
        #return to normal
        self.reset_control_modes()
        self.set_neutral()
        if not self._init_state:
            print("Disabling robot...")
            self._rs.disable()
        return True






def main():
    rospy.init_node("ik_pick_and_place_demo")
    # Starting Joint angles for left arm
    starting_joint_angles = {'left_w0': 0.6699952259595108,
                             'left_w1': 1.030009435085784,
                             'left_w2': -0.4999997247485215,
                             'left_e0': -1.189968899785275,
                             'left_e1': 1.9400238130755056,
                             'left_s0': -0.08000397926829805,
                             'left_s1': -0.9999781166910306}

    pnp = IK()
    #rospy.on_shutdown(test.clean_shutdown)
    overhead_orientation = Quaternion(
                             x=-0.0249590815779,
                             y=0.999649402929,
                             z=0.00737916180073,
                             w=0.00486450832011)
    block_poses = list()
    block_poses.append(Pose(
        position=Point(x=0.7, y=0.15, z=-0.129),
        orientation=overhead_orientation))
    # Feel free to add additional desired poses for the object.
    # Each additional pose will get its own pick and place.
    block_poses.append(Pose(
        position=Point(x=0.75, y=0.0, z=-0.129),
        orientation=overhead_orientation))

    pnp.move_to_start(starting_joint_angles)

    idx = 0
    while not rospy.is_shutdown():
        print("\nPicking...")
        pnp.pick(block_poses[idx])
        print("\nPlacing...")
        idx = (idx+1) % len(block_poses)
        pnp.place(block_poses[idx])
    return 0

if __name__ == '__main__':
    main()
